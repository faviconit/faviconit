## Faviconit :: free favicon & apple touch icon creator to all devices and browsers

faviconit creates favicons, apple touch icons and the HTML header to all devices and browsers.

it's free! all you have to do is upload an image! you can also choose a name and set a version to avoid browser cache.

### Copyright

Eduardo Russo
