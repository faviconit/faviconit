function displayErrorMessages(jsonErrors) {
    var $closeButton = $('<button>').attr('type', 'button').attr('data-dismiss', 'alert').addClass('close').append('&times;');
    var $errorDiv = $('<div>').addClass('alert alert-danger alert-dismissable').append($closeButton);

    for (var i in jsonErrors) {
        $errorDiv.append(jsonErrors[i] + '<br/>');
    }

    $('.alert.alert-danger.alert-dismissable').hide('fast');
    $('#formTitle').after($errorDiv);
}


