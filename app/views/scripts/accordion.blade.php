<script type='text/javascript'>
    (function () {
        var iconName = "{{ Input::old('iconName') }}",
            iconVersion = "{{ Input::old('iconVersion') }}",
            iconLocation = "{{ Input::old('iconLocation') }}",
            $iconName = $('#iconName'),
            $iconVersion = $('#iconVersion'),
            $iconLocation = $('#iconLocation'),
            $toggle = $('#advanced-toggle'),
            $icon = $('#advanced-toggle-icon');

        $('#advanced').on('hide.bs.collapse', function () {
            $toggle.text("{{ trans('faviconit.advancedAccordionColapsed') }}");
            $icon.attr('class', 'glyphicon glyphicon-chevron-down');
        });
        $('#advanced').on('hidden.bs.collapse', function () {
            iconName = $iconName.val();
            iconVersion = $iconVersion.val();
            iconLocation = $iconLocation.val();
            $iconName.val('');
            $iconVersion.val('');
            $iconLocation.val('');
        });
        $('#advanced').on('show.bs.collapse', function () {
            $toggle.text("{{ trans('faviconit.advancedAccordionExpanded') }}");
            $icon.attr('class', 'glyphicon glyphicon-chevron-up');
            $iconName.val(iconName);
            $iconVersion.val(iconVersion);
            $iconLocation.val(iconLocation);
        });
    })();
</script>