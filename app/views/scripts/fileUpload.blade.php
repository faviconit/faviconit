<script type='text/javascript'>
    $(document).ready(function () {
        $("#fileUploader").uploadFile({
            url: "{{ URL::to(LaravelLocalization::getCurrentLocale() . '/upload') }}",
            fileName: "iconFile",
            multiple: false,
            showDone: false,
            showStatusAfterSuccess: false,
            uploadButtonClass: "btn btn-primary file-upload",
            dragDropStr: "{{ trans('faviconit.formFileDragNDrop') }}",
            multiDragErrorStr: "{{ trans('faviconit.formFileMultiDragError') }}",
            abortStr: "{{ trans('faviconit.formFileAbortUpload') }}",
            onSubmit: function (files) {
                $('div#upload-mask').block({
                    message: '<p>{{ trans('faviconit.uploaderBlockMessage') }}</p>',
                    css: { border: 'none', background: 'none', color:'white', margin:'5px 0' }
                });
                $('#imageDisplay').children().remove();
                $('.alert.alert-danger.alert-dismissable').remove();
            },
            onSuccess: function (files, data, xhr) {
                $('div#upload-mask').unblock();
                $('.ajax-file-upload-statusbar').remove();

                var response = JSON.parse(data);
                var message = response.message;

                //Validation Error
                if (message == 'error') {
                    displayErrorMessages(response.error.iconFile);
                }
                else {
                    $('.file-upload').text("{{ trans('faviconit.swapImageText') }}");

                    var fullPath = "{{ URL::to('/') }}/" + response.path;

                    $('#imageDisplay').append($('<img>').attr('id', 'uploadedImage').attr('src', fullPath));
                    $('#imageDisplay').append($('<a>').attr('href', 'https://picresize.com/'));
                    // $('#imageDisplay').append($('<input>').addClass('btn btn-primary').attr('type', 'button').val("{{ trans('faviconit.editImageText') }}")
                    // .click(function () {
                    //     return launchEditor('uploadedImage', $('#aviaryIconImage').val());
                    // }));

                    $('#iconImage').val(fullPath);

                    // $('#aviaryIconImage').val(fullPath);

                    // if (message == 'notSquared') {
                    //     launchEditorWithCrop('uploadedImage', fullPath);
                    // }
                }
            },
            onError: function (files, status, errMsg) {
                $('div#upload-mask').unblock();
                $('.ajax-file-upload-statusbar').remove();
                alert("{{ trans('faviconit.uploadError') }}");
            }
        });
    });
</script>
