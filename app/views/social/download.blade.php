<hr class="divider"></hr>
<p class="lead">{{ trans('faviconit.shareTheLove') }}</p>
<div class="social-love">
<span>
    <a href="https://twitter.com/share" class="twitter-share-button" data-url="{{ Request::url() }}" data-via="faviconit_" data-count="none">Tweet</a>
</span>
<span>
    <div class="g-plus" data-action="share" data-annotation="none" data-href="{{ Request::url() }}"></div>
</span>
<span>
    <div class="fb-share-button" data-href="{{ Request::url() }}" data-type="button"></div>
</span>
</div>