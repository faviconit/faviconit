</div>
<footer class="navbar footer" role="navigation">
    <div class="navbar-inner">
        <?php /*
		<ul>
            <li>{{ HTML::link('about', "about") }}</li>
        </ul>
        */
        ?>
        <div class="copyright">
            © {{ CopyrightHelper::getYearString() }}
            {{ HTML::link('http://www.linkedin.com/in/russoedu', ' ' . ' Eduardo Russo', array('target' => '_blank')) }}
            &amp;
            {{ HTML::link('http://pedrohcan.com.br/', ' ' . ' Pedro H. Candido', array('target' => '_blank')) }}
        </div>
    </div>
</footer>