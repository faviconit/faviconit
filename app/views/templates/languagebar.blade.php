<a href="#" class="dropdown-toggle" data-toggle="dropdown">
    <div class="dropdown"></div>{{ $native }}<b class="caret"></b>
</a>
<ul class="dropdown-menu">
    @foreach($languages as $localeCode => $language)
    @if($localeCode == $active)
    <li class="active">
        <a hreflang="{{ $localeCode }}" href="#">
            <!-- <div class="flag flag-{{ $localeCode }}"></div> -->
            <span class="language">{{ $language['native'] }}</span>
        </a>
    </li>
    @else
    <li>
        <a rel="alternate" hreflang="{{ $localeCode }}"
           href="{{ LaravelLocalization::getLocalizedURL($localeCode) }}">
            <!-- <div class="flag flag-{{ $localeCode }}"></div> -->
            <span class="language">{{ $language['name'] }} - {{ $language['native'] }}</span>
        </a>
    </li>
    @endif
    @endforeach
    <li>
        <a rel="alternate" target="_blank" href="https://docs.google.com/forms/d/1BjjQoaa6utmm9ndYMsCO4QIj3C0IKN8UK5tAh55QXsA/viewform">
            <span class="language">{{ trans('faviconit.helpUsTranslate') }}</span>
        </a>
    </li>
</ul>
