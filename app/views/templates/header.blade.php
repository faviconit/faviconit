<body>
<nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
    <div class="navbar-header">
        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
        </button>
        <a href="{{ URL::to(LaravelLocalization::getCurrentLocale()) }}" class="brand">
            <img src="{{ asset('/assets/img/faviconit-logo.png') }}"/>
        </a>
    </div>
    <div class="navbar-collapse collapse navbar-responsive-collapse">
        <ul class="nav navbar-nav navbar-right">
            @include('social.header')
            <li class="dropdown">
                {{ LocalizationHelper::getLanguageSelector() }}
            </li>
        </ul>
    </div>
</nav>
<div class="container main">