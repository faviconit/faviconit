@if (App::environment() != 'local')
{{ HTML::script("//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js$ver") }}
<!-- {{ HTML::script("//feather.aviary.com/js/feather.js") }} -->
@else
{{ HTML::script("//ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js$ver") }}
@endif

@if (LaravelLocalization::getCurrentLocaleDirection() == 'ltr')
{{ HTML::script("//netdna.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js$ver") }}
@else
{{ HTML::script("//cdn.rtlcss.com/bootstrap/3.3.7/js/bootstrap.min.js$ver") }}
@endif

{{ HTML::script("//cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js$ver") }}
{{ HTML::script("/assets/js/faviconit.viewManipulation.js$ver") }}
{{ HTML::script("/assets/js/faviconit.imageEditor.js$ver") }}
{{ HTML::script("/assets/js/jquery.blockUI.js$ver") }}
{{ HTML::script("/assets/js/jquery.uploadfile.custom.js$ver") }}

@include('scripts.fileUpload')
@include('scripts.accordion')
@include('scripts.imageEditor')
@include('scripts.ga')
</body>
</html>
