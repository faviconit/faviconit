<!DOCTYPE html>
<html lang="{{ Lang::getLocale() }}" dir="{{ LaravelLocalization::getCurrentLocaleDirection() }}">
<head>
    <title>{{ trans('faviconit.pageTitle') }}</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    @if (isset($metaDescription))
    <meta name="Description" content="{{ $metaDescription }}">
    @endif
    <meta name="apple-mobile-web-app-title" content="faviconit!">
    <meta property="fb:admins" content="634567411" />
    <meta name="twitter:site:id" content="2318538170">
    <meta property="og:url" content="{{ Request::url() }}">
    <meta property="og:site_name" content="faviconit">
    <meta property="og:title" content="{{ trans('faviconit.ogTitle') }}">
    <meta property="og:description" content="{{ trans('faviconit.ogDescription') }}">
    @if (isset($socialImageUrl))
    <meta property="og:image" content="{{ $socialImageUrl }}">
    @else
    <meta property="og:image" content="{{ asset('assets/img/faviconit-social-logo-3.png') }}">
    @endif
    <meta property="og:locale" content="{{ LocalizationHelper::getSocialLocale() }}">
    <meta property="og:type" content="website">
    <meta name="twitter:card" content="summary">
    @if (LaravelLocalization::getCurrentLocaleDirection() == 'ltr')
    {{ HTML::style("//netdna.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css$ver") }}
    @else
    {{ HTML::style("//cdn.rtlcss.com/bootstrap/3.3.7/js/bootstrap.min.css$ver") }}
    @endif
    {{ HTML::style("/assets/css/bootstrap-themed.css$ver") }}
    {{ HTML::style("/assets/css/uploadfile.min.css$ver") }}
    {{ HTML::style("/assets/css/faviconit.css$ver") }}
    <!-- ****** faviconit.com favicons ****** -->
    <link rel="shortcut icon" href="/favicon.ico">
    <link rel="icon" sizes="16x16 32x32 64x64" href="/favicon.ico">
    <link rel="icon" type="image/png" sizes="196x196" href="/favicon-192.png">
    <link rel="icon" type="image/png" sizes="160x160" href="/favicon-160.png">
    <link rel="icon" type="image/png" sizes="96x96" href="/favicon-96.png">
    <link rel="icon" type="image/png" sizes="64x64" href="/favicon-64.png">
    <link rel="icon" type="image/png" sizes="32x32" href="/favicon-32.png">
    <link rel="icon" type="image/png" sizes="16x16" href="/favicon-16.png">
    <link rel="apple-touch-icon" href="/favicon-57.png">
    <link rel="apple-touch-icon" sizes="114x114" href="/favicon-114.png">
    <link rel="apple-touch-icon" sizes="72x72" href="/favicon-72.png">
    <link rel="apple-touch-icon" sizes="144x144" href="/favicon-144.png">
    <link rel="apple-touch-icon" sizes="60x60" href="/favicon-60.png">
    <link rel="apple-touch-icon" sizes="120x120" href="/favicon-120.png">
    <link rel="apple-touch-icon" sizes="76x76" href="/favicon-76.png">
    <link rel="apple-touch-icon" sizes="152x152" href="/favicon-152.png">
    <link rel="apple-touch-icon" sizes="180x180" href="/favicon-180.png">
    <meta name="msapplication-TileColor" content="#FFFFFF">
    <meta name="msapplication-TileImage" content="/favicon-144.png">
    <meta name="msapplication-config" content="/browserconfig.xml">
    <!-- ****** faviconit.com favicons ****** -->
    @include('scripts.social')
</head>
