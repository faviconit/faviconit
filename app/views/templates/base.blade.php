<?php
    $ver = '?v=' . Config::get('app.assetsVersion');
?>
@include('templates.html.header')
@include('templates.header')
@include('ads.horizontal-top')
@yield('content')
@include('ads.vertical-top')
@include('templates.footer')
@include('templates.html.footer')
