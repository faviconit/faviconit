@extends('templates.base')
@section('content')
<div class="col-lg-8 col-sm-12">
    <div class="jumbotron">
        <p class="highlight">{{ trans('faviconit.pageNotFound')}}</p>
        <h4>{{ HTML::link('/', trans('faviconit.goBackLink')) }} {{ trans('faviconit.goBackText') }}</h4>
    </div>
</div>
@endsection