@extends('templates.base')
@section('content')
<div class="col-lg-8 col-sm-12">
    <div class="span7">
        <!--        <div class="row well">-->
        <!--            -->
        <!--        </div>-->
        <div class="row well">
            <img class="faviconit-image" src="{{ asset('assets/img/faviconit.png') }}"/>

            <h3><span class="brand">faviconit</span> creates <i>favicons</i> and the <i>HTML header</i> to all devices
                and browsers.</h3>

            <p>it's free! all you have to do is upload a squared 200px image (or larger). you can also choose a name and
                set a version to avoid browser cache.</p>

        </div>
    </div>
</div>
@endsection