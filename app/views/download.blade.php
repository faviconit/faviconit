@extends('templates.base')
@section('content')
<div class="col-lg-8 col-sm-12">
    @if (isset($downloadUrl))
    <div class="row jumbotron">
        <p class="highlight">{{ trans('faviconit.downloadReady') }}</p>
        <h4>
            {{ trans('faviconit.donationButton') }}
            {{ trans('faviconit.thanks') }}
        </h4>
    </div>
    <div class="row">
        <a href="{{ $downloadUrl }}" role="button" class="btn btn-large btn-block btn-success">
            <span class="glyphicon glyphicon-cloud-download"></span>
            <span>{{ trans('faviconit.download') }}</span>
        </a>
    </div>
    <p class="small">{{ trans('faviconit.availability') }}</p>
    @include('social.download')
    <div class="row">
        <img class="img-responsive" src="{{ $socialImageUrl }}" />
    </div>
    @else
    <div class="jumbotron">
        <p class="highlight">{{ trans('faviconit.downloadNotFound') }}</p>
        <h4>{{ HTML::link('/', trans('faviconit.goBackLink')) }} {{ trans('faviconit.goBackText') }}</h4>
    </div>
    @endif
</div>
@endsection