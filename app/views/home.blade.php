@extends('templates.base')
@section('content')
<div class="col-lg-8 col-sm-12">
    <div class="row jumbotron">
        <p class="highlight">{{ trans('faviconit.homeHighlight') }}</p>
        <p>{{ trans('faviconit.homeSubHighlight') }}</p>
    </div>
    <div class="row well">
        {{ Form::open(array('url' => LaravelLocalization::getCurrentLocale() . '/faviconit', 'files' => true, 'class' => 'form-horizontal')) }}
        <legend id="formTitle" class="text-center">{{ trans('faviconit.fillTheForm') }}</legend>
        @if (Session::has('errors'))
        <div class="alert alert-danger alert-dismissable">
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            @foreach($errors->all() as $error)
            {{ $error }}</br>
            @endforeach
        </div>
        @endif
        <div class="form-group">
            <label class="control-label col-sm-3" for="iconFile">
                * {{ trans('faviconit.faviconFile') }}
            </label>
            <div class="col-sm-9">
                <div id="upload-mask">
                    <div id="fileUploader">{{ trans('faviconit.formFileChoose') }}</div>
                </div>
                <span class="help-block">
                    {{ trans('faviconit.formFileHelp') }}</strong>
                </span>
                <div id="imageDisplay"></div>
            </div>
        </div>
        <div class="panel-group" id="accordion">
            <div class="panel panel-default">
                <div id="advanced" class="panel-collapse collapse">
                    <div class="panel-body">
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="iconName">
                                {{ trans('faviconit.faviconName') }}
                            </label>
                            <div class="col-sm-9">
                                {{ Form::text('iconName', Input::old('iconName'), array('id' => 'iconName',
                                'class' => 'form-control input-lg form-control', 'autofocus' => 'autofocus', 'placeholder' =>
                                trans('faviconit.faviconNamePlaceholder'))) }}
                                <span class="help-block">
                                    {{ trans('faviconit.faviconNameHelp') }}
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="iconVersion">
                                {{ trans('faviconit.faviconVersion') }}
                            </label>
                            <div class="col-sm-9">
                                {{ Form::text('iconVersion', Input::old('iconVersion'), array('id' => 'iconVersion',
                                'class' => 'form-control input-lg form-control', 'placeholder' => trans('faviconit.faviconVersionPlaceholder'))) }}
                                <span class="help-block">
                                    {{ trans('faviconit.faviconVersionHelp') }}
                                </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-sm-3" for="iconLocation">
                                {{ trans('faviconit.faviconLocation') }}
                            </label>
                            <div class="col-sm-9">
                                {{ Form::text('iconLocation', Input::old('iconLocation'), array('id' => 'iconLocation',
                                'class' => 'form-control input-lg form-control', 'placeholder' => trans('faviconit.faviconLocationPlaceholder'))) }}
                                <span class="help-block">
                                    {{ trans('faviconit.faviconLocationHelp') }}
                                </span>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-heading">
                    <h4 class="panel-title">
                        <a data-toggle="collapse" data-parent="#accordion" href="#advanced" type="button"
                           class="btn btn-default btn-xs">
                            <span id="advanced-toggle-icon" class="glyphicon glyphicon-chevron-down"></span>
                            <span id="advanced-toggle">{{ trans('faviconit.advancedAccordionColapsed') }}</span>
                        </a>
                    </h4>
                </div>
            </div>
        </div>
        {{ Form::hidden('aviaryIconImage', '', array('id' => 'aviaryIconImage')) }}
        {{ Form::hidden('iconImage', '', array('id' => 'iconImage')) }}
        {{ Form::submit('faviconit!', array('id' => 'faviconit', 'class' => 'btn btn-lg btn-primary pull-right')) }}
        {{ Form::close() }}
    </div>
</div>
@endsection
