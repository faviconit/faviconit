<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="KU95JUNRHAUMN" />
        <input type="image" src="https://www.paypalobjects.com/it_IT/IT/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Fai una donazione con il pulsante PayPal" />
        <img alt="" border="0" src="https://www.paypal.com/it_IT/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "faviconit :: generatore gratuito di favicon e icone apple-touch per tutti i browser e dispositivi",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span> crea <i>favicon</i>, <i>icone apple-touch</i> e i rispettivi <i>header HTML</i> per tutti i browser e dispositivi.',
    'homeSubHighlight'              => "è gratis! il tool più facile da usare che tu abbia mai visto. tutto quello che devi fare è caricare un'immagine!",
    'fillTheForm'                   => 'riempi il form e crea la tua favicon gratis',
    'faviconName'                   => 'nome favicon',
    'faviconNamePlaceholder'        => 'lascia vuoto per usare "favicon" come nome',
    'faviconNameHelp'               => "sono ammessi solo numeri, lettere, '.', '-' e '_'",
    'faviconVersion'                => 'versione favicon',
    'faviconVersionPlaceholder'     => "lascia vuoto se non vuoi utilizzare questa funzione",
    'faviconVersionHelp'            => "sono ammessi solo numeri, lettere, '.', '-' e '_'",
    'faviconFile'                   => 'file favicon',
    'formFileChoose'                => 'scegli il file',
    'formFileDragNDrop'             => '<span>o trascinalo qui sopra</span>',
    'formFileMultiDragError'        => 'trascina un file solo',
    'uploaderBlockMessage'          => 'caricamento in corso…',
    'formFileAbortUpload'           => 'annulla',
    'formFileHelp'                  => "l'immagine deve essere <strong>almeno 310x310px</strong> e <strong>massimo 1MB</strong>",
    'swapImageText'                 => 'scambia immagine',
    'editImageText'                 => 'modifica immagine',
    'uploadError'                   => "ci dispiace, qualcosa è andato storto :( per favore, prova a caricare l'immagine di nuovo!",
    'advancedAccordionColapsed'     => 'avanzate',
    'advancedAccordionExpanded'     => 'base',
    'faviconLocation'               => 'cartella favicon',
    'faviconLocationPlaceholder'    => 'lascia vuoto per usare la cartella di root',
    'faviconLocationHelp'           => "sono ammessi solo numeri, lettere, ':', '.', e '/'",
    'donationButton'                => $donationButton,
    'downloadReady'                 => 'le tue favicon sono pronte per essere scaricate :)',
    'thanks'                        => 'grazie per aver usato <span class="brand">favicon<span class="orange">it</span></span> per creare le tue favicon, icone apple-touch e header HTML!',
    'download'                      => 'scarica la tue favicon',
    'availability'                  => "la tua favicon sarà disponibile per il download a questo indirizzo per un'ora",
    'downloadNotFound'              => "spiacente… non abbiamo trovato questa favicon nei nostri server",
    'goBackLink'                    => 'torna indietro',
    'goBackText'                    => "e carica un'immagine per creare favicon, icone apple-touch e gli header HTML",
    'shareTheLove'                  => 'facile, no? condividilo con i tuoi amici',
    'instructionsTitle'             => 'istruzioni-faviconit',
    'instructionThanks'             => 'grazie per aver usato faviconit!',
    'instructions'                  => "copia i file al tuo sito e aggiungi questo codice nel tag <HEAD> della pagina HTML:",
    'instructionsBlock'             => 'favicon faviconit.com',
    'helpUsTranslate'               => 'aiutaci a tradurre :)',
    'pageNotFound'                  => 'ops… la pagina che stavi cercando non è stata trovata',
    'uploadError'                   => "si è verificato un errore nel salvataggio dell'immagine. per favore, prova di nuovo.",
    'metaDescription'               => "carica un'immagine per creare favicon, icone apple-touch e i corretti header HTML per utilizzarle con tutti i browser e dispositivi",
    'metaDownload'                  => 'scarica il file zip con le favicon, le immagini apple-touch e le istruzioni degli header HTML',
    'metaFileNotFound'              => "il file non è disponibile. carica un'immagine per creare le favicon, le icone apple-touch e gli header HTML",
    'ogTitle'                       => 'generatore gratuito di favicon e icone apple-touch',
    'ogDescription'                 => "carica un'immagine per creare favicon, icone apple-touch e i corretti header HTML per utilizzarle con tutti i browser e dispositivi",
    'socialImage'                   => "generatore gratuito di favicon\ne icone apple-touch",
);
