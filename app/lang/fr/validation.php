<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    "accepted"           => "the :attribute must be accepted.",
    "active_url"         => "the :attribute is not a valid URL.",
    "after"              => "the :attribute must be a date after :date.",
    "alpha"              => "the :attribute may only contain letters.",
    "alpha_dash"         => "the :attribute may only contain letters, numbers, and dashes.",
    "alpha_num"          => "the :attribute may only contain letters and numbers.",
    "array"              => "the :attribute must be an array.",
    "before"             => "the :attribute must be a date before :date.",
    "between"            => array(
        "numeric" => "the :attribute must be between :min - :max.",
        "file"    => "the :attribute must be between :min - :max kilobytes.",
        "string"  => "the :attribute must be between :min - :max characters.",
        "array"   => "the :attribute must have between :min - :max items.",
    ),
    "confirmed"          => "the :attribute confirmation does not match.",
    "date"               => "the :attribute is not a valid date.",
    "date_format"        => "the :attribute does not match the format :format.",
    "different"          => "the :attribute and :other must be different.",
    "digits"             => "the :attribute must be :digits digits.",
    "digits_between"     => "the :attribute must be between :min and :max digits.",
    "email"              => "the :attribute format is invalid.",
    "exists"             => "the selected :attribute is invalid.",
    "in"                 => "the selected :attribute is invalid.",
    "integer"            => "the :attribute must be an integer.",
    "ip"                 => "the :attribute must be a valid IP address.",
    "mimes"              => "The :attribute must be a file of type: :values.",
    "min"                => array(
        "numeric" => "The :attribute must be at least :min.",
        "file"    => "The :attribute must be at least :min kilobytes.",
        "string"  => "The :attribute must be at least :min characters.",
        "array"   => "The :attribute must have at least :min items.",
    ),
    "not_in"             => "the selected :attribute is invalid.",
    "numeric"            => "the :attribute must be a number.",
    "required_if"        => ":attribute field is required when :other is :value.",
    "required_with"      => ":attribute field is required when :values is present.",
    "required_without"   => ":attribute field is required when :values is not present.",
    "same"               => ":attribute and :other must match.",
    "size"               => array(
        "numeric" => "The :attribute must be :size.",
        "file"    => "The :attribute must be :size kilobytes.",
        "string"  => "The :attribute must be :size characters.",
        "array"   => "The :attribute must contain :size items.",
    ),
    "unique"             => "The :attribute has already been taken.",
    "url"                => "The :attribute is required.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom'             => array(),

    /*
    |--------------------------------------------------------------------------
    | Fields in use
    |--------------------------------------------------------------------------
    */

    "max"                => array(
        "numeric" => "la valeur de :attribute ne peut être supérieure à :max.",
        "file"    => "le fichier :attribute ne peut être plus gros que :max kilobytes.",
        "string"  => "le texte de :attribute ne peut contenir plus de :max caractères.",
        "array"   => "le champ :attribute ne peut avoir plus de :max éléments.",
    ),
    "regex"              => "le format de :attribute est invalid.",
    "required"           => "le champ :attribute est obligatoire.",
    "image"              => "le champ :attribute doit être une image(jpeg, png, bmp, or gif).",
    "squared_image"      => "l':attribute doit être un <strong>carré</strong> dont le côté vaut au minimum 310 px. <a href='https://picresize.com/' target='_blamk'>CROP HERE</a>",
    // "squared_image"      => "l':attribute doit être un <strong>carré</strong> dont le côté vaut au minimum 310 px. téléchargez votre image et cliquez sur `éditer l'image' pour la rogner.",
    "minimum_size_image" => "les dimensions du <strong>fichier du favicon</strong> être au minimum de 310 px. merci de choisir une image plus grande.",

    'attributes'         => array(
        'iconName'     => '<strong>nom du favicon</strong>',
        'iconFile'     => '<strong>fichier du favicon</strong>',
        'iconVersion'  => '<strong>version du favicon</strong>',
        'iconImage'    => '<strong>fichier du favicon</strong>',
        'iconLocation' => '<strong>dossier du favicon</strong>',
    ),
);
