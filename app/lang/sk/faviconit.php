<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="A7ZYJFGJ94X5L" />
        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
        <img alt="" border="0" src="https://www.paypal.com/en_SK/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "faviconit :: bezplatný nástroj na tvorbu favicon & apple touch icon pre všetky zariadenia a prehliadače",
    'socialImage'                   => "bezplatný nástroj na tvorbu favicon &\napple touch icon pre všetky zariadenia a prehliadače",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span> vytvára <i>favicons</i>, <i>apple touch icons</i> a <i>HTML hlavičku</i> pre všetky zariadenia a prehlaidače',
    'homeSubHighlight'              => "je zdarma! najjednoduchší nástroj pre tvorbu favicons, aký ste kedy videli. jediné, čo potrebujete je nahrať obrázok!",
    'fillTheForm'                   => 'vyplnte formulár a vytvorte si svoje favicon zdarma',
    'faviconName'                   => 'favicon meno',
    'faviconNamePlaceholder'        => 'ponechajte prázdne, ak chcete použiť "favicon" ako meno',
    'faviconNameHelp'               => "povolené sú len písmená, čísla, '.', '-' a '_' ",
    'faviconVersionHelp'            => "povolené sú len písmená, čísla, '.', '-' a '_' ",
    'faviconLocationHelp'           => "povolené sú len písmená, čísla, ':', '.', and '/' allowed",
    'faviconVersion'                => 'favicon verzia',
    'faviconVersionPlaceholder'     => "ponechajte prázdne, ak túto možnosť nechcete využiť",
    'ogDescription'                 => 'nahrajte obrázok pre vytvorenie favicon, apple touch icon a HTML hlavičiek, fungujúce vo všetkých zariadeniach a prehliadačoch ',
    'metaDescription'               => 'nahrajte obrázok pre vytvorenie favicon, apple touch icon a HTML hlavičiek, fungujúce vo všetkých zariadeniach a prehliadačoch ',
    'metaDownload'                  => 'stiahnite zip súbor obsahujúci favicons, apple touch obrázky a inštrukcie k HTML hlavičkám',
    'metaFileNotFound'              => 'súbor nedostupný. nahrajte obrázok pre vytvorenie favicon, apple touch icon a HTML hlavičiek',
    'ogTitle'                       => 'bezplatný nástroj pre tvorbu favicon & apple touch icon',
    'faviconFile'                   => 'favicon súbor',
    'formFileChoose'                => 'vyberte súbor',
    'formFileDragNDrop'             => '<span>alebo ho chyťte a vložte sem</span>',
    'formFileMultiDragError'        => 'chyťte a vložte jeden súbor',
    'uploaderBlockMessage'          => 'nahrávam súbor…',
    'formFileAbortUpload'           => 'zrušiť',
    'formFileHelp'                  => 'obrázok musí mať <strong>minimálne 310x310px</strong> a <strong>maximálne 1MB</strong></strong>',
    'swapImageText'                 => 'zmeniť obrázok',
    'editImageText'                 => 'upraviť obrázok',
    'uploadError'                   => "prepáčte, niečo je nesprávne :( prosím, skúste nahrať svoj obrázok znova!",
    'advancedAccordionColapsed'     => 'pokročilé',
    'advancedAccordionExpanded'     => 'štandardné',
    'uploadError'                   => 'chyba pri pokuse o uloženie obrázku. prosím, skúste to znova.',
    'downloadReady'                 => 'vaše favicons sú pripravené :)',
    'thanks'                        => 'ďakujeme za použitie faviconit pre tvorbu favicons, apple touch icons a HTML hlavičiek!',
    'download'                      => 'stiahnuť moje favicons',
    'availability'                  => 'vaše favicons budú dostupné na stiahnutie minimálne 1 hodinu na tejto URL',
    'downloadNotFound'              => "prepáčte… nemôžeme nájsť túto favicon na našich serveroch",
    'goBackLink'                    => 'ísť späť',
    'goBackText'                    => 'a nahrať obrázok pre vytvorenie favicons, apple touch icons a HTML hlavičiek',
    'faviconLocation'               => 'favicon adresár',
    'faviconLocationPlaceholder'    => 'ponechajte prázdne, ak chcete nechať favicon v root adresári',
    'shareTheLove'                  => 'jednoduché, však? zdieľajte lásku s priateľmi',
    'pageNotFound'                  => 'ups… stránkú, ktorú hľadáte nevieme nájsť',
    'instructionsTitle'             => 'faviconit-inštrukcie',
    'instructionThanks'             => 'ďakujeme za používanie faviconit!',
    'instructions'                  => "skopírujte súbory do root adresáru vašej stránky a pridajte tento kód do HTML <HEAD> tagu:",
    'instructionsBlock'             => 'faviconit.com favicons',
    'helpUsTranslate'               => 'pomôžte nám s prekladom :)',

    'donationButton'                => $donationButton,
);
