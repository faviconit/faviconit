<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="CUU5S4MNYJ4GA" />
        <input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Botón Donar con PayPal" />
        <img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "faviconit :: creador de favicons i apple touch per a tots els dispositius i navegadors",
    'socialImage'                   => "creador de favicons i apple touch\nper a tots els dispositius i navegadors",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span> crea <i>favicons</i>, <i>icones d\'apple touch</i> i les <i>etiquetes HTML</i> per a tots els dispositius i navegadors.',
    'homeSubHighlight'              => "és gratuït! la manera més fàcil de crear favicons que has vist mai. tot el que has de fer és pujar una imatge!",
    'fillTheForm'                   => 'emplena el formulari i crea el teu favicon gratis',
    'faviconName'                   => 'nom del favicon',
    'faviconNamePlaceholder'        => 'deixa-ho en blanc per utilitzar "favicon" com a nom',
    'faviconNameHelp'               => "només es permeten lletres, números, punts i els guions - i _",
    'faviconVersionHelp'            => "només es permeten lletres, números, punts i els guions - i _",
    'faviconLocationHelp'           => "només es permeten lletres, números, ':', '.', i '/'",
    'faviconVersion'                => 'versió del favicon',
    'faviconVersionPlaceholder'     => "deixa-ho en blanc si no ho vols utilitzar",
    'ogDescription'                 => 'puja una imatge per crear favicons, apple touch i l\'etiqueta HTML per fer-ho funcionar en tots els dispositius i navegadors',
    'metaDescription'               => 'puja una imatge per crear favicons, apple touch i l\'etiqueta HTML per fer-ho funcionar en tots els dispositius i navegadors',
    'metaDownload'                  => 'descarrega el fitxer .zip amb els favicons, les imatges apple touch i les etiquetes HTML',
    'metaFileNotFound'              => 'el fitxer no està disponible. Penja una imatge per crear favicons, imatges d\'apple touch i les etiquetes HTML',
    'ogTitle'                       => 'creador de favicons i icones d\'apple touch gratuït',
    'faviconFile'                   => 'fitxer del favicon',
    'formFileChoose'                => 'tria el fitxer',
    'formFileDragNDrop'             => '<span>o arrossega\'l aquí</span>',
    'formFileMultiDragError'        => 'arrossega un sol fitxer aquí',
    'uploaderBlockMessage'          => 'penjant fitxer…',
    'formFileAbortUpload'           => 'cancel·lar',
    'formFileHelp'                  => 'l\'imatge ha de ser <strong>al mínim de 200x200px</strong> i ha de pesar <strong>màxim 1MB</strong>',
    'swapImageText'                 => 'intercanviar imatge',
    'editImageText'                 => 'editar imatge',
    'uploadError'                   => "disculpi, alguna cosa ha anat malament :( si us plau, prova-ho penjant la imatge una altre vegada!",
    'advancedAccordionColapsed'     => 'avançat',
    'advancedAccordionExpanded'     => 'regular',
    'uploadError'                   => 'hi ha hagut un error mentre desàvem la imatge. intenta-ho una altre vegada, si us plau.',
    'downloadReady'                 => 'els teus favicons estan a punt :)',
    'thanks'                        => 'gràcies per utilitzar <span class="brand">favicon<span class="orange">it</span></span> per crear els teus favicos, apple touch icons i les etiquetes HTML!',
    'download'                      => 'descarregar els meus favicons',
    'availability'                  => 'els favicons estaran disponibles per descarregar 1 hora en aquest URL',
    'downloadNotFound'              => "Disculpi… no hem trobat aquest favicon als nostres servidors",
    'goBackLink'                    => 'torna',
    'goBackText'                    => 'i penja una imatge per crear favicons, apple touch icons i les etiquetes HTML',
    'faviconLocation'               => 'carpeta del favicon',
    'faviconLocationPlaceholder'    => 'deixa-ho en blanc si vols mantenir el favicon en la carpeta arrel',
    'shareTheLove'                  => 'fàcil, oi? comparteix el teu amor amb els teus amics!',
    'pageNotFound'                  => 'ups... la pàgina que busques no s\'ha trobat',
    'instructionsTitle'             => 'instruccions-faviconit',
    'instructionThanks'             => 'gràcies per utilitzar faviconit!',
    'instructions'                  => "copia els fitxers a l'arrel de la teva pàgina web i insereix l'etiqueta HTML a dintre de l'etiqueta <HEAD>:",
    'instructionsBlock'             => 'faviconit.com favicons',
    'helpUsTranslate'               => 'ajuda\'ns a traduir :)',

    'donationButton'                => $donationButton,
);
