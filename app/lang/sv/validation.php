<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */
    "accepted"         => ":attribute måste accepteras.",
    "active_url"       => ":attribute är inte en giltig webbadress.",
    "after"            => ":attribute måste vara ett datum efter den :date.",
    "alpha"            => ":attribute får endast innehålla bokstäver.",
    "alpha_dash"       => ":attribute får endast innehålla bokstäver, nummer och bindestreck.",
    "alpha_num"        => ":attribute får endast innehålla bokstäver och nummer.",
    "array"            => ":attribute måste vara en array.",
    "before"           => ":attribute måste vara ett datum innan den :date.",
    "between"          => array(
        "numeric" => ":attribute måste vara ett nummer mellan :min och :max.",
        "file"    => ":attribute måste vara mellan :min till :max kilobytes stor.",
        "string"  => ":attribute måste innehålla :min till :max tecken.",
        "array"   => ":attribute måste innehålla mellan :min - :max objekt."
    ),
    "confirmed"        => ":attribute bekräftelsen matchar inte.",
    "date"             => ":attribute är inte ett giltigt datum.",
    "date_format"      => ":attribute matchar inte formatet :format.",
    "different"        => ":attribute och :other får inte vara lika.",
    "digits"           => ":attribute måste vara minst :digits tecken.",
    "digits_between"   => ":attribute måste vara mellan :min och :max tecken.",
    "email"            => "Fältet :attribute måste innehålla en korrekt e-postadress.",
    "exists"           => "Det valda :attribute är ogiltigt.",
    "in"               => "Det valda :attribute är ogiltigt.",
    "integer"          => ":attribute måste vara en siffra.",
    "ip"               => ":attribute måste vara en giltig IP-adress.",
    "mimes"            => ":attribute måste vara en fil av typen: :values.",
    "min"              => array(
        "numeric" => ":attribute måste vara större än :min.",
        "file"    => ":attribute måste minst vara :min kilobytes stor.",
        "string"  => ":attribute måste minst innehålla :min tecken.",
        "array"   => ":attribute måste minst innehålla :min objekt."
    ),
    "not_in"           => "Det valda :attribute är ogiltigt.",
    "numeric"          => ":attribute måste vara ett nummer.",
    "required_if"      => "Fältet :attribute är obligatoriskt då :other är :value.",
    "required_with"    => "Fältet :attribute är obligatoriskt då :values är ifyllt.",
    "required_with_all" => "Fältet :attribute är obligatoriskt när :values är ifyllt.",
    "required_without" => "Fältet :attribute är obligatoriskt då :values ej är ifyllt.",
    "required_without_all" => "Fältet :attribute är obligatoriskt när ingen av :values är ifyllt.",
    "same"             => ":attribute och :other måste vara lika.",
    "size"             => array(
        "numeric" => ":attribute måste vara :size.",
        "file"    => ":attribute får endast vara :size kilobyte stor.",
        "string"  => ":attribute måste innehålla :size tecken.",
        "array"   => ":attribute måste innehålla :size objekt."
    ),
    "unique"           => ":attribute används redan.",
    "url"              => ":attribute formatet är ogiltig.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => array(
        'attribute-name' => array(
            'rule-name' => 'custom-message',
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | Fields in use
    |--------------------------------------------------------------------------
    */

    "max"              => array(
        "numeric" => ":attribute får inte vara större än :max.",
        "file"    => ":attribute får max vara :max kilobytes stor.",
        "string"  => ":attribute får max innehålla :max tecken.",
        "array"   => ":attribute får inte innehålla mer än :max objekt."
    ),
    "regex"            => "Formatet för :attribute är ogiltigt.",
    "required"         => ":attribute fältet är obligatoriskt.",
    "image"            => ":attribute måste vara en bild.",
    "squared_image"      => ":attribute måste vara <strong>kvadratisk</strong> med minst 310px. <a href='https://picresize.com/' target='_blamk'>CROP HERE</a>",
    // "squared_image"      => ":attribute måste vara <strong>kvadratisk</strong> med minst 310px. Ladda upp din bild och klicka på 'redigera bild' för att beskära den.",
    "minimum_size_image" => "<strong>favicon-filen</strong> måste vara på minst 310px i bägge riktningarna. var vänlig ladda upp en större bild.",
    'attributes'         => array(
        'iconName'     => '<strong>favicon-namn</strong>',
        'iconVersion'  => '<strong>favicon-version</strong>',
        'iconFile'     => '<strong>favicon-fil</strong>',
        'iconImage'    => '<strong>favicon-fil</strong>',
        'iconLocation' => '<strong>favicon-mapp (katalog)</strong>',
    ),

);
