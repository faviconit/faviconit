<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="7SV5QBFR5JSRC" />
        <input type="image" src="https://www.paypalobjects.com/sv_SE/SE/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donera med PayPal-knappen" />
        <img alt="" border="0" src="https://www.paypal.com/sv_SE/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "faviconit :: gratis favicon- och apple touch ikon-skapare för alla apparater och webbläsare",
    'socialImage'                   => "gratis favicon- och apple touch\nikon-skapare för alla apparater och webbläsare",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span>faviconit skapar <i>favicon-er</i>, <i>apple touch-ikoner</i> och <i>HTML-koden</i> för alla apparater och webbläsare',
    'homeSubHighlight'              => "det är gratis! den enklast möjliga favicon-skaparen. du behöver bara ladda upp en bild",
    'fillTheForm'                   => 'fyll i formuläret och skapa din favicon gratis',
    'faviconName'                   => 'favicon-namn',
    'faviconNamePlaceholder'        => 'lämna tomt för att använda namnet "favicon"',
    'faviconNameHelp'               => "bara bokstäver, siffror, '.', '-' och '_'  är tillåtna",
    'faviconVersionHelp'            => "bara bokstäver, siffror, '.', '-' och '_'  är tillåtna",
    'faviconLocationHelp'           => "bara bokstäver, siffror, ':', '.' och '/'  är tillåtna",
    'faviconVersion'                => 'favicon-version',
    'faviconVersionPlaceholder'     => "lämnas tomt om du inte önskar använda detta",
    'ogDescription'                 => 'ladda upp en bild för att skapa ikoner (favicons, apple touch icons) och den HTML-kod som behövs för att de skall fungera med alla apparater och webbläsare',
    'metaDescription'               => 'ladda upp en bild för att skapa ikoner (favicons, apple touch icons) och den HTML-kod som behövs för att de skall fungera med alla apparater och webbläsare',
    'metaDownload'                  => 'ladda ner en zip-fil med favicon-er, apple touch-bilder och HTML-instruktioner',
    'metaFileNotFound'              => 'det saknas en fil. ladda upp en bild för att skapa faviconer, apple touch-bilder och HTML-kod',
    'ogTitle'                       => 'gratis favicon- och apple touch ikon-skapare',
    'faviconFile'                   => 'favicon-fil',
    'formFileChoose'                => 'välj fil',
    'formFileDragNDrop'             => '<span>eller, släpa-och-släpp här</span>',
    'formFileMultiDragError'        => 'släpa-och-släpp en (1) fil',
    'uploaderBlockMessage'          => 'filen laddas upp…',
    'formFileAbortUpload'           => 'avbryt',
    'formFileHelp'                  => 'bilden måste vara <strong>åtminstone 310x310px</strong> och på <strong>max 1MB</strong>',
    'swapImageText'                 => 'byt bild',
    'editImageText'                 => 'redigera bild',
    'uploadError'                   => "hoppsan, något blev fel :( var vänlig försök att ladda upp din bild igen!",
    'advancedAccordionColapsed'     => 'avancerat',
    'advancedAccordionExpanded'     => 'vanligt',
    'uploadError'                   => 'försöket att spara bilden misslyckades. var snäll försök igen.',
    'downloadReady'                 => 'dina favicon-er är klara för avhämtning :)',
    'thanks'                        => 'Tack för att du använder faviconit för att skapa dina favicon-er, apple touch-ikoner och HTML-koden!',
    'download'                      => 'ladda ner mina favicons',
    'availability'                  => 'dina faviconer kommer att finnas tillgängliga för nedladdning i minst 1 timme på denna URL',
    'downloadNotFound'              => "tyvärr… vi kunde inte hitta denna favicon på våra servrar",
    'goBackLink'                    => 'gå tillbaka',
    'goBackText'                    => 'gå tillbaka och ladda upp en bild för att skapa favicon-er, apple touch-ikoner och HTML-koden',
    'faviconLocation'               => 'favicon-mapp (katalog)',
    'faviconLocationPlaceholder'    => 'lämnas tom för att behålla favicon-en i rot-mappen (-katalogen)',
    'shareTheLove'                  => 'enkelt, eller hur? dela din uppskattning med dina vänner',
    'pageNotFound'                  => 'hoppsan… sidan som du sökte kunde inte hittas',
    'instructionsTitle'             => 'faviconit-instruktioner',
    'instructionThanks'             => 'Tack för att du använer faviconit!',
    'instructions'                  => "kopiera filerna till din sajt-rot och lägg till denna kod inom HTML <HEAD>-taggen:",
    'instructionsBlock'             => 'faviconit.com favicon-er',
    'helpUsTranslate'               => 'hjälp oss att översätta',

    'donationButton'                => $donationButton,
);
