<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="XTUHKF2L7L8CE" />
        <input type="image" src="https://www.paypalobjects.com/en_US/GB/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
        <img alt="" border="0" src="https://www.paypal.com/en_GB/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "faviconit :: free favicon & apple touch icon generator for all devices and browsers",
    'socialImage'                   => "free favicon & apple touch icon\ngenerator for all devices and browsers",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span> generates <i>favicons</i>, <i>apple touch icons</i> and the <i>HTML header</i> to all devices and browsers.',
    'homeSubHighlight'              => "it's free! the easiest favicon creator you'll ever see. all you have to do is upload an image!",
    'fillTheForm'                   => 'fill the form and create your favicon for free',
    'faviconName'                   => 'favicon name',
    'faviconNamePlaceholder'        => 'leave blank to use "favicon" as name',
    'faviconNameHelp'               => "only letters, numbers, '.', '-' and '_' allowed",
    'faviconVersionHelp'            => "only letters, numbers, '.', '-' and '_' allowed",
    'faviconLocationHelp'           => "only letters, numbers, ':', '.', and '/' allowed",
    'faviconVersion'                => 'favicon version',
    'faviconVersionPlaceholder'     => "leave blank if you don't want to use this",
    'ogDescription'                 => 'upload an image to generate favicons, apple touch icons and the correct HTML header to make it work with all devices and browsers',
    'metaDescription'               => 'upload an image to generate favicons, apple touch icons and the correct HTML header to make it work with all devices and browsers',
    'metaDownload'                  => 'download the zip file with the favicons, apple touch images and the HTML header instructions',
    'metaFileNotFound'              => 'file is not available. upload an image do create the favicons, apple touch images and the HTML header',
    'ogTitle'                       => 'free favicon & apple touch icon generator',
    'faviconFile'                   => 'favicon file',
    'formFileChoose'                => 'choose file',
    'formFileDragNDrop'             => '<span>or, drag &amp; drop it here</span>',
    'formFileMultiDragError'        => 'drag &amp; drop a single file',
    'uploaderBlockMessage'          => 'uploading file…',
    'formFileAbortUpload'           => 'abort',
    'formFileHelp'                  => 'image must be <strong>at least 310x310px</strong> with <strong>maximum 1MB</strong>',
    'swapImageText'                 => 'swap image',
    'editImageText'                 => 'edit image',
    'uploadError'                   => "sorry, something was wrong :( please, try uploading your image again!",
    'advancedAccordionColapsed'     => 'advanced',
    'advancedAccordionExpanded'     => 'regular',
    'uploadError'                   => 'error when trying to save the image. please, try again.',
    'downloadReady'                 => 'your favicons are ready to go :)',
    'thanks'                        => 'thank you for using <span class="brand">favicon<span class="orange">it</span></span> to create your favicons, apple touch icons and the html header!',
    'download'                      => 'download my favicons',
    'availability'                  => 'your favicons will be available to download for at least 1 hour in this URL',
    'downloadNotFound'              => "sorry… we couldn't find this favicon in our servers",
    'goBackLink'                    => 'go back',
    'goBackText'                    => 'and upload an image to to create favicons, apple touch icons and the HTML header',
    'faviconLocation'               => 'favicon folder',
    'faviconLocationPlaceholder'    => 'leave blank to use the root folder',
    'shareTheLove'                  => 'easy, right? share the love with your friends',
    'pageNotFound'                  => 'ops… the page you are looking for could not be found',
    'instructionsTitle'             => 'faviconit-instructions',
    'instructionThanks'             => 'thanks for using faviconit!',
    'instructions'                  => "copy the files to your site and add this code inside the HTML <HEAD> tag:",
    'instructionsBlock'             => 'faviconit.com favicons',
    'helpUsTranslate'               => 'help us translating :)',

    'donationButton'                => $donationButton,
);
