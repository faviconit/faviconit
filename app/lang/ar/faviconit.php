<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="VJZY9BF439DCC" />
        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
        <img alt="" border="0" src="https://www.paypal.com/en_EG/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "ايقونه مجانية وانشاء ايقونات apple touch لجميع الاجهزه والمتصفحات :: faviconit",
    'socialImage'                   => "ايقونه مجانية وانشاء ايقونات apple touch لجميع الاجهزه والمتصفحات",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span> منشئ <i>ايقونه</i>, <i>apple touch ايقونات</i> و <i>HTML header</i> لجميع الاجهزه والمتصفحات.',
    'homeSubHighlight'              => "انه مجانا! أسهل منشئ للأيقونات سوف تراه , كل ما عليك فعله رفع الصورة!",
    'fillTheForm'                   => 'اكمل النموذج و أنشاء أيقونتك مجانا',
    'faviconName'                   => 'اسم الايقونه',
    'faviconNamePlaceholder'        => 'ااترك الاسم خالي لاستخدام اسم "الايقونه"',
    'faviconNameHelp'               => "فقظ حروف , أرقام , '.' , '-' ,  '_' مسموح",
    'faviconVersionHelp'            => "فقظ حروف , أرقام , '.' , '-' ,  '_' مسموح",
    'faviconLocationHelp'           => "فقظ حروف , أرقام , ':' , '.' ,  '/' مسموح",
    'faviconVersion'                => 'اصدار الايقونه',
    'faviconVersionPlaceholder'     => "اتركه خالي اذا كنت لا تريد استخدامه",
    'ogDescription'                 => 'قم بتحميل صورة لإنشاء الايقونات وايضا يمكنك انشاء ايقونات touch apple و HTML header الصحيح لجعلها تعمل مع جميع الأجهزة والمتصفحات',
    'metaDescription'               => 'قم بتحميل صورة لإنشاء الايقونات وايضا يمكنك انشاء ايقونات touch apple و HTML header الصحيح لجعلها تعمل مع جميع الأجهزة والمتصفحات',
    'metaDownload'                  => 'تحميل الملف المضغوط (zip) يحتوي علي الايقونات و  صور apple touch و ارشادات Html header',
    'metaFileNotFound'              => 'لم يتم رفع الملف , ارفع صوره لكي تنشئ الايقونه , صور apple touch و html header',
    'ogTitle'                       => 'ايكونات مجانيه و منشئ ايقونات apple touch',
    'faviconFile'                   => 'ملف الايقونه',
    'formFileChoose'                => 'اختر ملف',
    'formFileDragNDrop'             => '<span>أو ، اسحبها وإفلاتها هنا</span>',
    'formFileMultiDragError'        => 'سحب  وافلات ملف واحد',
    'uploaderBlockMessage'          => 'رفع الملف ...',
    'formFileAbortUpload'           => 'الغاء',
    'formFileHelp'                  => 'يجب ان تكون الصوره <strong>على الاقل 200x200px</strong> مع <strong>اعلى احد 1MB',
    'swapImageText'                 => 'تبديل الصوره',
    'editImageText'                 => 'تعديل الصوره',
    'uploadError'                   => "عذرا! ، حدث خطأ ما : (يرجى رفع الصور مجددا)",
    'advancedAccordionColapsed'     => 'متقدم',
    'advancedAccordionExpanded'     => 'عادي',
    'uploadError'                   => 'حدث خطأ عند محاوله حفظ الصورة, يرجى لحاوله مره اخرى',
    'downloadReady'                 => 'أيقونتك جاهزه (:',
    'thanks'                        => 'شكرًا لك على استخدام faviconit لإنشاء الأيقونات و  touch apple و  Html header',
    'download'                      => 'تحميل الايقونات',
    'availability'                  => 'ايقوناتك سوف تكون متوفره للتحميل لمده ساعه كامله على هذا الرابط',
    'downloadNotFound'              => "عذرا ... لم نستطيع اجاد الايقونه في السيرفر",
    'goBackLink'                    => 'رجوع للخلف',
    'goBackText'                    => 'وتحميل صورة لإنشاء أيقونات و touch apple و Html Header ',
    'faviconLocation'               => 'مجلد الايقونه',
    'faviconLocationPlaceholder'    => 'اتركه خالي لكي تجعل الايقونه في المجلد الرئيسي',
    'shareTheLove'                  => 'سهل ، صحيح ؟ انشر المحبه بين اصدقائك',
    'pageNotFound'                  => 'اوبس ... الصفحه التي تبحث عنها غير متوفره ',
    'instructionsTitle'             => 'faviconit - التعليمات',
    'instructionThanks'             => 'شكرا لاستخدامك faviconit (:',
    'instructions'                  => "انسخ الملفات إلى موقعك الرئيسي وضع الكود التالي في <head> تاق",
    'instructionsBlock'             => 'faviconit.com الايقونات',
    'helpUsTranslate'               => 'ساعدنا في الترجمه (:',

    'donationButton'                => $donationButton,
);
