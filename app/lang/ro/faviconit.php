<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="CK2ZN94GZC4DG" />
        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
        <img alt="" border="0" src="https://www.paypal.com/en_RO/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return [
    'pageTitle'                     => "faviconit :: favicon gratis & creator de imagini apple touch pentru toate dispozitivele și browsere",
    'socialImage'                   => "favicon gratis & creator de imagini\napple touch pentru toate dispozitivele și browsere",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span> creaza <i>favicons</i>, <i>imagini apple touch</i> și <i>codul HTML</i> pentru toate dispozitivele și browsere.',
    'homeSubHighlight'              => "este gratis! cel mai simplu creatorul favicon care îl veți vedea vreodată. tot ce trebuie să faceți este să încărcați o imagine!",
    'fillTheForm'                   => 'completați formularul pentru a crea un favicon gratuit',
    'faviconName'                   => 'numele favicon',
    'faviconNamePlaceholder'        => 'lăsați liber ca sa utilizați "favicon" ca nume',
    'faviconNameHelp'               => "numai litere, numere, '.', '-' și '_' sunt autorizate",
    'faviconVersionHelp'            => "numai litere, numere, '.', '-' și '_' sunt autorizate",
    'faviconLocationHelp'           => "numai litere, numere, ':', '.' și '/' sunt autorizate",
    'faviconVersion'                => 'versiunea favicon',
    'faviconVersionPlaceholder'     => "lăsați liber dacă nu vreți sa îl utilizați",
    'ogDescription'                 => 'încărcați o imagine ca sa creați favicons, imagini apple touche și codul HTML corect ca sa funcționeze cu toate navigatoarele internet',
    'metaDescription'               => 'încărcați o imagine ca sa creați favicons, imagini apple touche și codul HTML corect ca sa funcționeze cu toate navigatoarele internet',
    'metaDownload'                  => 'descărcați fișierul cu faviocons, imagini apple touche si codul HTML cu instrucțiunile',
    'metaFileNotFound'              => 'fișier nu este disponibil. încărcați o imagine ca sa creați un favicon, imagini apple touch si codul HTML',
    'ogTitle'                       => 'favicon gratis & creator imagini apple touch',
    'faviconFile'                   => 'fișierul favicon',
    'formFileChoose'                => 'alegeți un fișier',
    'formFileDragNDrop'             => '<span>sau, trageți & depozați aici</span>',
    'formFileMultiDragError'        => 'trageți & depozați-un singur fișier',
    'uploaderBlockMessage'          => 'fișierul se încarcă¬',
    'formFileAbortUpload'           => 'abandonați',
    'formFileHelp'                  => 'imaginea trebuie sa fie <strong>minimum 310x310px</strong> cu <strong>maximum 1MB</strong>',
    'swapImageText'                 => 'inversați imaginea',
    'editImageText'                 => 'modificați imaginea',
    'uploadError'                   => "ne pare rău, ceva nu a mers :( vă rog, încercați din nou imaginea!",
    'advancedAccordionColapsed'     => 'avansat',
    'advancedAccordionExpanded'     => 'regular',
    'uploadError'                   => 'eroare la încercarea de a salva imaginea. vă rugam, încercați din nou.',
    'downloadReady'                 => 'favicons este gata :)',
    'thanks'                        => 'vă mulțumim pentru ca ați utilizat <span class="brand">favicon<span class="orange">it</span></span> pentru a crea favicons, imagini apple touch și codul HTML!',
    'download'                      => 'descarca favicons',
    'availability'                  => 'favicons va fi disponibil pentru descărcare pentru cel puțin o oră în această adresă URL',
    'downloadNotFound'              => "scuze… nu am putut găsi această favicon în serverele noastre",
    'goBackLink'                    => 'mergeți înapoi',
    'goBackText'                    => 'și încărcați o imagine pentru a crea favicons, imagini apple touche si codul HTML',
    'faviconLocation'               => 'dosarul favicon',
    'faviconLocationPlaceholder'    => 'lăsați liber pentru a păstra favicon în dosarul principal',
    'shareTheLove'                  => 'ușor, nu? împărtășiți dragostea cu prietenii tăi',
    'pageNotFound'                  => 'hopa ... pagina pe care o cautați nu a putut fi găsită',
    'instructionsTitle'             => 'instrucții-faviconit',
    'instructionThanks'             => 'va mulțumim pentru ca utilizați faviconit!',
    'instructions'                  => "copiati fișierele loa rădăcina site-ului dvs. și adăugați acest cod în interiorul tagului HTML <HEAD>:",
    'instructionsBlock'             => 'faviconit.com favicons',
    'helpUsTranslate'               => 'ajutatine sa traducem :)',
    'donationButton'                => $donationButton,
];
