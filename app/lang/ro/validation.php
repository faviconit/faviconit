<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */
    "accepted"         => "Câmpul :attribute trebuie să fie acceptat.",
    "active_url"       => "Câmpul :attribute nu este un URL valid.",
    "after"            => "Câmpul :attribute trebuie să fie o dată după :date.",
    "alpha"            => "Câmpul :attribute poate conține numai litere.",
    "alpha_dash"       => "Câmpul :attribute poate conține numai litere, numere și liniuțe.",
    "alpha_num"        => "Câmpul :attribute poate conține numai litere și numere.",
    "array"            => "Câmpul :attribute trebuie să fie un array.",
    "before"           => "Câmpul :attribute trebuie să fie o dată înainte de :date.",
    "between"          => array(
        "numeric" => "Câmpul :attribute trebuie să fie între :min și :max.",
        "file"    => "Câmpul :attribute trebuie să fie între :min și :max kilobiți.",
        "string"  => "Câmpul :attribute trebuie să fie între :min și :max caractere.",
        "array"   => "Câmpul :attribute trebuie să aibă :min - :max elemente."
    ),
    "confirmed"        => "Confirmarea :attribute nu se potrivește.",
    "date"             => "Câmpul :attribute nu este o dată validă.",
    "date_format"      => "Câmpul :attribute trebuie să fie intr-un format valid.",
    "different"        => "Campurile :attribute și :other trebuie să fie diferite.",
    "digits"           => "Câmpul :attribute trebuie să fie de :digits cifre.",
    "digits_between"   => "Câmpul :attribute trebuie să fie între :min și :max cifre.",
    "email"            => "Formatul câmpului :attribute este invalid.",
    "exists"           => "Câmpul :attribute selectat este invalid.",
    "in"               => "Câmpul :attribute selectat este invalid.",
    "integer"          => "Câmpul :attribute trebuie să fie un număr întreg.",
    "ip"               => "Câmpul :attribute trebuie să fie o adresă IP validă.",
    "max"              => array(
        "numeric" => "Câmpul :attribute trebuie să fie mai mic de :max.",
        "file"    => "Câmpul :attribute trebuie să fie mai mic de :max kilobiți.",
        "string"  => "Câmpul :attribute trebuie să fie mai mic de :max caractere.",
        "array"   => "Câmpul :attribute nu poate avea mai mult de :max elemente."
    ),
    "mimes"            => "Câmpul :attribute trebuie să fie un fișier de tipul: :values.",
    "min"              => array(
        "numeric" => "Câmpul :attribute trebuie să fie cel puțin :min.",
        "file"    => "Câmpul :attribute trebuie să aibă cel puțin :min kilobiți.",
        "string"  => "Câmpul :attribute trebuie să aibă cel puțin :min caractere.",
        "array"   => "Câmpul :attribute trebuie să aibă cel puțin :min elemente."
    ),
    "not_in"           => "Câmpul :attribute selectat este invalid.",
    "numeric"          => "Câmpul :attribute trebuie să fie un număr.",
    "required_if"      => "Câmpul :attribute este necesar atunci când :other e :value.",
    "required_with"    => "Câmpul :attribute este necesar atunci când :values este prezent.",
    "required_with_all" => "The :attribute field is required when :values is present.",
    "required_without" => "Câmpul :attribute este necesar atunci când :values nu este prezent.",
    "required_without_all" => "Câmpul :attribute este necesar atunci când nici una din valorile :values ne este prezentă.",
    "same"             => "Câmpul :attribute și :other trebuie să fie identice.",
    "size"             => array(
        "numeric" => "Câmpul :attribute trebuie să fie :size.",
        "file"    => "Câmpul :attribute trebuie să aibă :size kilobyte.",
        "string"  => "Câmpul :attribute trebuie să aibă :size caractere.",
        "array"   => "Câmpul :attribute trebuie sa conțină :size elemente."
    ),
    "unique"           => "Câmpul :attribute a fost deja folosit.",
    "url"              => "Câmpul :attribute nu este într-un format valid.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom' => array(
        'attribute-name' => array(
            'rule-name' => 'custom-message',
        ),
    ),

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Attributes
    |--------------------------------------------------------------------------
    |
    | The following language lines are used to swap attribute place-holders
    | with something more reader friendly such as E-Mail Address instead
    | of "email". This simply helps us make messages a little cleaner.
    |
    */

    'attributes' => array(),

    /*
   |--------------------------------------------------------------------------
   | Fields in use
   |--------------------------------------------------------------------------
   */

    "max"              => array(
        "numeric" => "câmpul :attribute trebuie să fie mai mic de :max.",
        "file"    => "câmpul :attribute trebuie să fie mai mic de :max kilobiți.",
        "string"  => "câmpul :attribute trebuie să fie mai mic de :max caractere.",
        "array"   => "câmpul :attribute nu poate avea mai mult de :max elemente."
    ),

    "regex"            => "formatul câmpului :attribute este invalid.",
    "required"         => "fâmpul :attribute este obligatoriu.",
    "image"            => "câmpul :attribute trebuie să fie o imagine.",

    "squared_image"      => ":atribute trebuie să fie un <strong>pătrat</strong> de cel puțin 310px. <a href='https://picresize.com/' target='_blamk'>CROP HERE</a>",
    // "squared_image"      => ":atribute trebuie să fie un <strong>pătrat</strong> de cel puțin 310px. încărca imaginea și faceți clic pe 'modificați imaginea' pentru a decupa.",
    "minimum_size_image" => "<strong>fișierul favicon</strong> trebuie să aibă cel puțin 310px în fiecare dimensiune. vă rugam să încărcați o imagine mai mare.",

    'attributes'         => array(
        'iconName'     => '<strong>numele favicon</strong>',
        'iconFile'     => '<strong>fișierul favicon</strong>',
        'iconImage'    => '<strong>fișierul favicon</strong>',
        'iconVersion'  => '<strong>versiunea favicon</strong>',
        'iconLocation' => '<strong>dosarul favicon</strong>',
    ),

);
