<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="6MW54GV9MWQ3W" />
        <input type="image" src="https://www.paypalobjects.com/pl_PL/PL/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Przekaż darowiznę za pomocą przycisku PayPal" />
        <img alt="" border="0" src="https://www.paypal.com/pl_PL/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "faviconit :: darmowy kreator favicon, ikon apple touch - na wszystkie urządzenia i przeglądarki",
    'socialImage'                   => "darmowy kreator favicon, ikon apple touch -\nna wszystkie urządzenia i przeglądarki",
    'homeHighlight'                 => "<span class=\"brand\">favicon<span class=\"orange\">it</span></span> tworzy <i>favicony</i>, <i>ikony apple touch</i> i <i>nagłówki html</i> dla wszystkich urządzeń i przeglądarek",
    'homeSubHighlight'              => "bezpłatnie! najprostszy kreator favicon, jaki widziałeś. wszystko, co musisz zrobić to wgranie obrazka!",
    'fillTheForm'                   => 'wypełnij formularz i utwórz swoją faviconę za darmo',
    'faviconName'                   => 'nazwa favicony',
    'faviconNamePlaceholder'        => 'pozostaw puste by użyć "favicon" jako nazwę',
    'faviconNameHelp'               => "dozwolone tylko litery, cyfry, '.', '-' i '_'",
    'faviconVersionHelp'            => "dozwolone tylko litery, cyfry, '.', '-' i '_'",
    'faviconLocationHelp'           => "dozwolone tylko litery, cyfry, ':', '.', i '/' allowed",
    'faviconVersion'                => 'wersja favicony',
    'faviconVersionPlaceholder'     => "pozostaw puste, jeśli nie chcesz tego używać",
    'ogDescription'                 => 'wgraj grafikę, by utworzyć faviconę, ikonę apple touch i poprawny nagłówek HTML działający na wszystkich urządzeniach i przeglądarkach',
    'metaDescription'               => 'wgraj grafikę, by utworzyć faviconę, ikonę apple touch i poprawny nagłówek HTML działający na wszystkich urządzeniach i przeglądarkach',
    'metaDownload'                  => 'ściągnij plik zip zawierający faviconę, ikonę apple touch i nagłówek HTML z instrukcją',
    'metaFileNotFound'              => 'plik nie jest dostępny. wgraj grafikę, by utworzyć faviconę, ikonę apple touch i nagłówek HTML',
    'ogTitle'                       => 'darmowy kreator favicon i ikon apple touch',
    'faviconFile'                   => 'plik favicony',
    'formFileChoose'                => 'wybierz plik',
    'formFileDragNDrop'             => '<span>lub przeciągnij i upuść go tutaj</span>',
    'formFileMultiDragError'        => 'przeciągnij i upuść pojedynczy plik',
    'uploaderBlockMessage'          => 'wgrywanie pliku…',
    'formFileAbortUpload'           => 'przerwij',
    'formFileHelp'                  => 'grafika musi mieć <strong>przynajmniej 310x310px</strong> i <strong>maksymalnie 1MB</strong>',
    'swapImageText'                 => 'podmień grafikę',
    'editImageText'                 => 'edytuj grafikę',
    'uploadError'                   => "przepraszamy, coś poszło nie tak :( spróbuj wgrać twoją grafikę ponownie!",
    'advancedAccordionColapsed'     => 'zaawansowane',
    'advancedAccordionExpanded'     => 'proste',
    'uploadError'                   => 'błąd podczas próby zapisania grafiki. spróbuj ponownie.',
    'downloadReady'                 => 'twoje favicony są gotowe :)',
    'thanks'                        => "dziękujemy za użycie <span class=\"brand\">favicon<span class=\"orange\">it</span></span> do przygotowania twoich favicon, ikon apple touch i nagłówka html!",
    'download'                      => 'ściągnij moje favicony',
    'availability'                  => 'twoje favicony będą dostępne do ściągnięcia przez przynajmniej 1 godzinę, pod tym adresem URL',
    'downloadNotFound'              => "przepraszamy... nie możemy znaleźć twojej favicony na naszych serwerach",
    'goBackLink'                    => 'wróć',
    'goBackText'                    => 'i wgraj swoją grafikę by utworzyć faviconę, ikonę apple touch i nagłówki html',
    'faviconLocation'               => 'folder favicony',
    'faviconLocationPlaceholder'    => 'pozostaw puste, jeśli chcesz zachować faviconę w głównym folderze',
    'shareTheLove'                  => 'łatwo, nie? podzieli się tym ze swoimi przyjaciółmi',
    'pageNotFound'                  => 'ops... strona, której szukasz nie została odnaleziona',
    'instructionsTitle'             => 'faviconit-instrukcje',
    'instructionThanks'             => 'dziękujemy za użycie faviconit!',
    'instructions'                  => "skopiuj te pliki do katalogu głównego swojej strony i dodaj poniższy kod do tagu <HEAD> w pliku HTML:",
    'instructionsBlock'             => 'favicony faviconit.com',
    'helpUsTranslate'               => 'pomóż nam tłumaczyć :)',
    'donationButton'                => $donationButton,
);
