<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines contain the default error messages used by
    | the validator class. Some of these rules have multiple versions such
    | such as the size rules. Feel free to tweak each of these messages.
    |
    */

    "accepted"           => "o campo :attribute deve ser aceite.",
    "active_url"         => "o campo :attribute não é uma URL válida.",
    "after"              => "o campo :attribute deve ser uma data após :date.",
    "alpha"              => "o campo :attribute só pode conter letras.",
    "alpha_dash"         => "o campo :attribute só pode conter letras, números e traços.",
    "alpha_num"          => "o campo :attribute só pode conter letras e números.",
    "array"              => "o campo :attribute deve ser uma lista.",
    "before"             => "o campo :attribute deve ser uma data anterior à :date.",
    "between"            => array(
        "numeric" => "o campo :attribute deve estar entre :min - :max.",
        "file"    => "o campo :attribute deve estar entre :min - :max kilobytes.",
        "string"  => "o campo :attribute deve estar entre :min - :max caracteres.",
        "array"   => "o campo :attribute deve estar entre :min - :max itens.",
    ),
    "confirmed"          => "o campo :attribute confirmação não coincide.",
    "date"               => "o campo :attribute não é uma data válida.",
    "date_format"        => "o campo :attribute não corresponde ao formato :format.",
    "different"          => "o campo :attribute e :other deve ser diferente.",
    "digits"             => "o campo :attribute deve ter :digits dígitos.",
    "digits_between"     => "o campo :attribute deve ter entre :min e :max dígitos.",
    "email"              => "o campo :attribute não é um e-mail válido.",
    "exists"             => "o campo :attribute selecionado é inválido.",
    "in"                 => "o campo :attribute selecionado é inválido.",
    "integer"            => "o campo :attribute deve ser um inteiro.",
    "ip"                 => "o campo :attribute deve ser um endereço IP válido.",
    "mimes"              => "O campo :attribute deve ser um arquivo do tipo: :values.",
    "min"                => array(
        "numeric" => "o campo :attribute deve conter pelo menos :min.",
        "file"    => "o campo :attribute deve conter pelo menos :min kilobytes.",
        "string"  => "o campo :attribute deve conter pelo menos :min caracteres.",
        "array"   => "o campo :attribute deve conter pelo menos :min itens.",
    ),
    "not_in"             => "o campo :attribute selecionado é inválido.",
    "numeric"            => "o campo :attribute deve ser um número.",
    "required_if"        => "o campo :attribute deve ser preenchido quando :other é :value.",
    "required_with"      => "o campo :attribute deve ser preenchido quando :values está presente.",
    "required_without"   => "o campo :attribute deve ser preenchido quando :values não está presente.",
    "same"               => "o campo :attribute e :other devem ser iguais.",
    "size"               => array(
        "numeric" => "o campo :attribute deve ser :size.",
        "file"    => "o campo :attribute deve ter :size kilobyte.",
        "string"  => "o campo :attribute deve ter :size caracteres.",
        "array"   => "o campo :attribute deve ter :size itens.",
    ),
    "unique"             => "o campo :attribute já existe.",
    "url"                => "o formato do campo :attribute é inválido.",

    /*
    |--------------------------------------------------------------------------
    | Custom Validation Language Lines
    |--------------------------------------------------------------------------
    |
    | Here you may specify custom validation messages for attributes using the
    | convention "attribute.rule" to name the lines. This makes it quick to
    | specify a specific custom language line for a given attribute rule.
    |
    */

    'custom'             => array(),

    /*
    |--------------------------------------------------------------------------
    | Fields in use
    |--------------------------------------------------------------------------
    */

    "max"                => array(
        "numeric" => "el campo :attribute debe ser menor que :max.",
        "file"    => "el campo :attribute debe ser menor que :max kilobytes.",
        "string"  => "el campo :attribute debe ser menor que :max caracteres.",
    ),
    "required"           => "el campo :attribute es requerido",
    "image"              => "el campo :attribute debe ser una imagen.",
    "regex"              => "el formato :atribute no es válido.",
    "squared_image"      => "el :attribute tiene que ser <strong>square</strong> con 310px mínimo. <a href='https://picresize.com/' target='_blamk'>CROP HERE</a>",
    // "squared_image"      => "el :attribute tiene que ser <strong>square</strong> con 310px mínimo. suba su imagen y haga click en 'editar imagen' para cortarla.",
    "minimum_size_image" => "el <strong>favicon</strong> tiene que ser con 310px mínimo en cada sentido. por favor, suba una imagen mayor.",

    'attributes'         => array(
        'iconName'     => '<strong>nombre del favicon</strong>',
        'iconFile'     => '<strong>archivo favicon</strong>',
        'iconVersion'  => '<strong>versión favicon</strong>',
        'iconImage'    => '<strong>archivo favicon</strong>',
        'iconLocation' => '<strong>carpeta del favicon</strong>',
    ),

);
