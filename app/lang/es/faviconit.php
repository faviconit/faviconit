<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="J4GAUUUSTU924" />
        <input type="image" src="https://www.paypalobjects.com/es_ES/ES/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Botón Donar con PayPal" />
        <img alt="" border="0" src="https://www.paypal.com/es_ES/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "faviconit :: creador gratis de favicon & apple touch icons para todos los dispositivos y navegadores",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span> crear <i>faviconos</i>, <i>apple touch icons</i> y el <i>HTML header</i> para todos los dispositivos y navegadores',
    'homeSubHighlight'              => "es gratis! el creador de favicon más sencillo ya visto! lo único que tienes que hacer es subir una imagen!",
    'fillTheForm'                   => 'complete el formulario y cree gratis su favicon',
    'faviconName'                   => 'nombre del favicon',
    'faviconNamePlaceholder'        => 'deje en blanco para usar "favicon" como nombre',
    'faviconNameHelp'               => "usar apenas letras, números, '.', '-'y '_'.",
    'faviconVersion'                => 'versión favicon',
    'faviconVersionPlaceholder'     => "deje en blanco se no lo quiere usar",
    'faviconVersionHelp'            => "usar apenas letras, números, '.', '-'y '_'.",
    'faviconFile'                   => 'archivo favicon',
    'formFileChoose'                => 'seleccione el archivo',
    'formFileDragNDrop'             => '<span>o arrastre y solte</span>',
    'formFileMultiDragError'        => 'arrastre y solte un solo archivo',
    'uploaderBlockMessage'          => 'cargando archivo…',
    'formFileAbortUpload'           => 'abortar',
    'formFileHelp'                  => 'la imagen debe ser <strong> de 310x310px mínimo<strong> con <strong> un máximo de 1MB',
    'swapImageText'                 => 'cambiar imagen',
    'editImageText'                 => 'editar imagen',
    'uploadError'                   => "lo siento, algo no funcionó",
    'advancedAccordionColapsed'     => 'avanzado',
    'advancedAccordionExpanded'     => 'regular',
    'faviconLocation'               => 'carpeta del favicon',
    'faviconLocationPlaceholder'    => 'deje en blanco para mantener el favicon en la carpeta raíz',
    'faviconLocationHelp'           => "usar apenas letras, números, ':', '.', y '/'",
    'donationButton'                => $donationButton,
    'downloadReady'                 => 'sus faviconos están listos :)',
    'thanks'                        => 'gracias por usar <span class="brand">favicon<span class="orange">it</span></span> para crear sus faviconos, apple touch icons y HTML header!',
    'download'                      => 'descargar mis faviconos',
    'availability'                  => 'tus faviconos estarán disponibles para descarga por 1 hora por lo menos en este URL',
    'downloadNotFound'              => "lo siento… no encontramos este favicon en nuestros servidores",
    'goBackLink'                    => 'vuelva',
    'goBackText'                    => 'y suba una imagen para crear favicon, apple touch icons y el HTML header',
    'shareTheLove'                  => 'fácil, no? comparta con tus amigos',
    'instructionsTitle'             => 'faviconit-instrucciones',
    'instructionThanks'             => 'gracias por usar faviconit!',
    'instructions'                  => "copie los archivos en su sitio y ponga este código dentro del HTML <HEAD> tag:",
    'instructionsBlock'             => 'faviconit.com faviconos',
    'helpUsTranslate'               => 'ayúdanos a traducir :)',
    'pageNotFound'                  => 'ops… la página que está buscando no existe',
    'uploadError'                   => 'hubo un error al salvar la imagen. por favor, intente de nuevo.',
    'metaDescription'               => 'suba una imagen para crear faviconos, apple touch icons y el HTML header correcto para que funcione en todos los dispositivos y navegadores',
    'metaDownload'                  => 'descargue el archivo zip con los faviconos, apple touch icons y las instrucciones del HTML header',
    'metaFileNotFound'              => 'archivo no disponible, suba una imagen para crear faviconos, apple touch icons y el HTML header correcto',
    'ogTitle'                       => 'creador gratis de favicon & apple touch icons',
    'ogDescription'                 => 'suba una imagen para crear faviconos, apple touch icons y el HTML header correcto para que funcione en todos los dispositivos y navegadores',
    'socialImage'                   => "creador gratis de favicon & apple touch icons\npara todos los dispositivos y navegadores",
);
