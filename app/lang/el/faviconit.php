<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="HWDUNWGR6V6VE" />
        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
        <img alt="" border="0" src="https://www.paypal.com/en_GR/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "Faviconit :: Δωρεάν δημιουργία favicon & apple touch εικονιδίου για όλες τις συσκευές και περιηγητές",
    'socialImage'                   => "Δωρεάν δημιουργία favicon & apple touch εικονιδίου για όλες τις συσκευές και περιηγητές",
    'homeHighlight'                 => 'To <span class="brand">favicon<span class="orange">it</span></span> δημιουργεί <i>favicons</i>, <i>apple touch εικονίδια</i> και την <i>HTML κεφαλίδα</i> σε όλες τις συσκευές και όλους τους περιηγητές.',
    'homeSubHighlight'              => "Είναι δωρεάν! ο ευκολότερος δημιουργός favicon που έχετε δει. το μόνο που έχετε να κάνετε είναι να ανεβάσετε μία εικόνα!",
    'fillTheForm'                   => 'Συμπληρώστε την φόρμα και δημιουργήστε to favicon σας δωρεάν',
    'faviconName'                   => 'Όνομα favicon',
    'faviconNamePlaceholder'        => 'Αφήστε το πεδίο κενό για να χρησιμοποιηθεί το "favicon" ώς όνομα ',
    'faviconNameHelp'               => "Μόνο γράμματα, αριθμοί, '.', '-' και '_' επιτρέπονται",
    'faviconVersionHelp'            => "Μόνο γράμματα, αριθμοί, '.', '-' και '_' επιτρέπονται",
    'faviconLocationHelp'           => "Μόνο γράμματα, αριθμοί, ':', '.', και '/' επιτρέπονται",
    'faviconVersion'                => 'favicon έκδοση',
    'faviconVersionPlaceholder'     => "Αφήστε το πεδίο κενό εάν δεν επιθυμείτε να να το χρησιμοποιήσετε αυτό",
    'ogDescription'                 => 'Ανεβάστε μία εικόνα για να δημιουργήσετε favicons, apple touch εικονίδια και την σωστή HTML κεφαλίδα ώστε να δουλέψει  σε όλες τις συσκευές και όλους τους περιηγητές ',
    'metaDescription'               => 'Ανεβάστε μία εικόνα για να δημιουργήσετε favicons, apple touch εικονίδια και την σωστή HTML κεφαλίδα ώστε να δουλέψει  σε όλες τις συσκευές και όλους τους περιηγητές ',
    'metaDownload'                  => 'Κατεβάστε το συμπιεσμένο αρχείο με τα favicon, apple touch εικονίδια και  τις οδηγιες την HTML κεφαλίδα',
    'metaFileNotFound'              => 'Το αρχείο δεν είναι διαθέσιμο. ανεβάστε μία εικόνα για να δημιουργήσετε favicons, apple touch εικονίδια και την HTML κεφαλίδα',
    'ogTitle'                       => 'Δωρεάν δημιουργία favicon & apple touch εικονιδίων',
    'faviconFile'                   => 'favicon αρχείο',
    'formFileChoose'                => 'Επιλέξτε ένα αρχείο',
    'formFileDragNDrop'             => '<span>Ή σύρετε τοποθετήστε το εδώ</span>',
    'formFileMultiDragError'        => 'Σύρετε & τοποθετήστε ένα μεμονωμένο αρχείο',
    'uploaderBlockMessage'          => 'Ανέβασμα αρχείου…',
    'formFileAbortUpload'           => 'Διακοπή',
    'formFileHelp'                  => 'Η εικόνα πρέπει να είναι <strong>τουλάχιστων 310x310px</strong> μέ <strong>ανώτατο όριο 1MB</strong></strong>',
    'swapImageText'                 => 'Αλλάξτε την εικόνα ',
    'editImageText'                 => 'Επεξεργαστείτε την εικόνα',
    'uploadError'                   => "Λυπούμαστε κάτι πήγε στραβά :( παρακαλώ, δοκιμάστε να ανεβάσετε την εικόνα σας ξανά!",
    'advancedAccordionColapsed'     => 'Προχωρημένες',
    'advancedAccordionExpanded'     => 'Απλές',
    'uploadError'                   => 'Λάθος κατα την προσπάθεια αποθήκευσης της εικόνας. παρακαλώ, δοκιμάστε ξανά.',
    'downloadReady'                 => 'Τα favicon σας είναι έτοιμα :)',
    'thanks'                        => 'Σας ευχαριστούμε που χρησιμοποιείτε το faviconit για την δημιουργία των favicon σας, apple touch εικονιδίων σας και τών HTML κεφαλίδων σας!',
    'download'                      => 'Κατέβασμα των favicon μου',
    'availability'                  => 'Τα favicon σας θα είναι διαθέσιμα  για κατέβασμα για τουλάχιστον 1 ώρα σε αυτό URL',
    'downloadNotFound'              => "Λυπούμαστε… δεν μπορέσαμε να βρούμε αυτό το favicon στον server μας ",
    'goBackLink'                    => 'Πηγαίντε πίσω',
    'goBackText'                    => 'και ανεβάστε μία εικόνα για να δημιουργήσετε  favicons, apple touch εικονίδια και την HTML κεφαλίδα',
    'faviconLocation'               => 'favicon φάκελος',
    'faviconLocationPlaceholder'    => 'Αφήστε το πεδίο κενό εάν επιθυμείτε να κρατήσετε το favicon σας στον ρυζικό φάκελο',
    'shareTheLove'                  => 'Εύκολο σωστα; δημοσιεύστε την αγάπη σας για εμάς με τους φίλους σας',
    'pageNotFound'                  => 'ουπς… η σελίδα που ψάχνετε  δεν βρέθηκε',
    'instructionsTitle'             => 'faviconit-Οδηγίες',
    'instructionThanks'             => 'Ευχαριστούμε που χρησιμοποιήσατε το faviconit!',
    'instructions'                  => "Αντιγράψτε αυτά αρχεία στην ρυζικη ιστοσελίδα σας και προσθέστε αυτόν τον κώδικα μέσα στην HTML <HEAD> ετικέτα:",
    'instructionsBlock'             => 'faviconit.com favicons',
    'helpUsTranslate'               => 'Βοηθήστε μας να μεταφράσουμε :)',

    'donationButton'                => $donationButton,
);
