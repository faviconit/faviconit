<?php

return array(
    "max"                => array(
        "numeric" => "o campo :attribute deve ser inferior a :max.",
        "file"    => "o campo :attribute deve ser inferior a :max kilobytes.",
        "string"  => "o campo :attribute deve ser inferior a :max caracteres.",
        "array"   => "o campo :attribute deve ser inferior a :max itens.",
    ),
    "required"           => "o campo :attribute deve ser preenchido.",
    "image"              => "o campo :attribute deve ser uma imagem.",
    "regex"              => "o campo :attribute não é válido.",

    "squared_image"      => "o :attribute precisa ser <strong>quadrado</strong> com pelo menos 310px. <a href='https://picresize.com/' target='_blamk'>CROP HERE</a>",
    // "squared_image"      => "o :attribute precisa ser <strong>quadrado</strong> com pelo menos 310px. carregue sua imagem novamente e clique em 'editar imagem' para cortá-la.",
    "minimum_size_image" => "o <strong>arquivo do favicon</strong> precisa ter pelo menos 310px em cada dimensão. por favor, carregue uma imagem maior.",

    'attributes'         => array(
        'iconName'     => '<strong>nome do favicon</strong>',
        'iconFile'     => '<strong>arquivo do favicon</strong>',
        'iconVersion'  => '<strong>versão do favicon</strong>',
        'iconImage'    => '<strong>arquivo do favicon</strong>',
        'iconLocation' => '<strong>pasta do favicon</strong>',
    ),
);
