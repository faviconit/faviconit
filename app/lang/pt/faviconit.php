<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="GFQDAB3ZVVY64" />
        <input type="image" src="https://www.paypalobjects.com/pt_BR/BR/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Faça doações com o botão do PayPal" />
        <img alt="" border="0" src="https://www.paypal.com/pt_BR/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "faviconit :: criador grátis de favicon & apple touch icon para todos dispositivos e navegadores",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span> cria <i>favicons</i>, <i>apple touch icons</i> e o <i>header do HTML</i> para todos dispositivos e navegadores.',
    'homeSubHighlight'              => "é grátis! o criador de favicons mais fácil que você já viu. tudo que você tem que fazer é carregar uma imagem!",
    'fillTheForm'                   => 'preencha o formulário e crie seu favicon de graça',
    'faviconName'                   => 'nome do favicon',
    'faviconNamePlaceholder'        => 'deixe em branco para usar "favicon" como nome',
    'faviconNameHelp'               => "apenas letras, números, '.', '-' e '_' são permitidos",
    'faviconVersion'                => 'versão do favicon',
    'faviconVersionPlaceholder'     => "deixe em branco se não quiser versionar",
    'faviconVersionHelp'            => "apenas letras, números, '.', '-' e '_' são permitidos",
    'faviconFile'                   => 'arquivo do favicon',
    'formFileChoose'                => 'escolher arquivo',
    'formFileDragNDrop'             => '<span>ou, arraste e solte aqui</span>',
    'formFileMultiDragError'        => 'arraste e solte apenas um arquivo',
    'uploaderBlockMessage'          => 'carregando arquivo…',
    'formFileAbortUpload'           => 'cancelar',
    'formFileHelp'                  => 'imagem precisa ter <strong>pelo menos 310x310px</strong> com <strong>no máximo 1MB',
    'swapImageText'                 => 'trocar imagem',
    'editImageText'                 => 'editar imagem',
    'uploadError'                   => "ops… algo deu errado :( por favor, carregue sua imagem novamente",
    'advancedAccordionColapsed'     => 'avançado',
    'advancedAccordionExpanded'     => 'regular',
    'faviconLocation'               => 'pasta do favicon',
    'faviconLocationPlaceholder'    => 'deixe em branco se o favicon for ficar na raiz do site',
    'faviconLocationHelp'           => "apenas letras, números, ':', '.', e '/' são permitidos",
    'donationButton'                => $donationButton,
    'downloadReady'                 => 'seu favicon está pronto :)',
    'thanks'                        => 'obrigado por usar o <span class="brand">favicon<span class="orange">it</span></span> para criar seus favicons, apple touch icons e o header do html!',
    'download'                      => 'baixar meus favicons',
    'availability'                  => 'seus favicons estarão disponíveis por pelo menos 1 hora nessa URL',
    'downloadNotFound'              => "foi mal… não encontramos esse favicon nos nossos servidores",
    'goBackLink'                    => 'volte',
    'goBackText'                    => 'e carregue uma imagem para criar favicons, apple touch icons e o header do HTML!',
    'shareTheLove'                  => 'fácil, né? compartilhe com seus amigos',
    'instructionsTitle'             => 'instrucoes-faviconit',
    'instructionThanks'             => "obrigado por usar o faviconit!",
    'instructions'                  => "copie os arquivo para seu site e adicione esse código dentro da tag <HEAD> do HTML",
    'instructionsBlock'             => 'favicons do faviconit.com',
    'helpUsTranslate'               => 'ajude-nos a traduzir :)',
    'pageNotFound'                  => 'ops… a página que você está procurando não foi encontrada',
    'uploadError'                   => 'erro ao salvar a imagem. por favor, tente novamente.',
    'metaDescription'               => 'carregue uma imagem para criar favicons, apple touch icons e o header de HTML correto para funcionarem em todos dispositivos e navegadores',
    'metaDownload'                  => 'baixe o ZIP com os favicons, apple touch images e a instrução para colocar o header HTML',
    'metaFileNotFound'              => 'arquivo não está disponível. carregue uma imagem para criar os favicons, apple touch images e o header do HTML',
    'ogTitle'                       => 'criador grátis de favicon & apple touch icon',
    'ogDescription'                 => 'carregue uma imagem para criar favicons, apple touch icons e o header de HTML correto para funcionarem em todos dispositivos e navegadores',
    'socialImage'                   => "criador grátis de favicon & apple touch icon\npara todos dispositivos e navegadores",
);
