<?php
$donationButton = <<<BTN
<div class="paypal-donate">
    <form action="https://www.paypal.com/cgi-bin/webscr" method="post" target="_top">
        <input type="hidden" name="cmd" value="_s-xclick" />
        <input type="hidden" name="hosted_button_id" value="8CCYC56FRCYXQ" />
        <input type="image" src="https://www.paypalobjects.com/en_US/i/btn/btn_donateCC_LG.gif" border="0" name="submit" title="PayPal - The safer, easier way to pay online!" alt="Donate with PayPal button" />
        <img alt="" border="0" src="https://www.paypal.com/en_FI/i/scr/pixel.gif" width="1" height="1" />
    </form>
</div>
BTN;
return array(
    'pageTitle'                     => "faviconit :: ilmainen favicon & apple touch ikonien luoja kaille laitteille ja selaimille",
    'homeHighlight'                 => '<span class="brand">favicon<span class="orange">it</span></span> faviconit luo <i>faviconeja</i>, <i>apple touch ikonien</i> ja <HTML tunnisteen</i> kaikille laiteille ja selaimille.',
    'homeSubHighlight'              => "kiitos kun käytit faviconit joka luo sinulle faviconeja, apple touch ikonien ja html määritteen!",
    'fillTheForm'                   => 'täytä kaavake ja luo faviconisi ilmaiseksi',
    'faviconName'                   => 'faviconin nimi',
    'faviconNamePlaceholder'        => 'jätä tyhjäksi jos haluat nimeksi "favicon"',
    'faviconNameHelp'               => "vain kirjaimet, numerot, '.', '_' ja  '_' ovat sallittuja",
    'faviconVersion'                => 'faviconin versio',
    'faviconVersionPlaceholder'     => "jätä tyhjäksi jos et halua käyttää tätä",
    'faviconVersionHelp'            => "vain kirjaimet, numerot, '.', '_' ja  '_' ovat sallittuja",
    'faviconFile'                   => 'favicon tiedosto',
    'formFileChoose'                => 'valitse tiedosto',
    'formFileDragNDrop'             => '<span>tai, vedä se tähän</span>',
    'formFileMultiDragError'        => 'vedä yksi tiedosto',
    'uploaderBlockMessage'          => 'ladataan…',
    'formFileAbortUpload'           => 'keskeytetty',
    'formFileHelp'                  => 'kuvan täytyy olla <strong> ainakin 310x310 pikseliä</strong> ja <strong> korkeintaan 1 megan kokoinen</strong>',
    'swapImageText'                 => 'vaihda kuva',
    'editImageText'                 => 'muokkaa kuvaa',
    'uploadError'                   => "hupsis, jotain meni vikaan :( yritä uudelleen!",
    'advancedAccordionColapsed'     => 'kehittynyt',
    'advancedAccordionExpanded'     => 'normaali',
    'faviconLocation'               => 'faviconin kansio',
    'faviconLocationPlaceholder'    => 'jätä tyhjäksi jos haluat käyttää root kansiota',
    'faviconLocationHelp'           => "vain kirjaimet, numerot, ':', '.', ja '/' ovat sallittuja",
    'donationButton'                => $donationButton,
    'downloadReady'                 => 'Faviconisi ovat valmiitä :)',
    'thanks'                        => 'kiitos kun käytit <span class="brand">favicon<span class="orange">it</span></span> joka luo sinulle faviconeja, apple touch ikonien ja html määritteen!',
    'download'                      => 'lataa minun kuvat',
    'availability'                  => 'kuvasi voidaan ladata ainakin tunnin ajan tästä url osoitteesta',
    'downloadNotFound'              => "hups... emme voineet löytää kuviasi meidän palvelistamme",
    'goBackLink'                    => 'palaa takaisin',
    'goBackText'                    => 'ja lataa kuva jotta voit luoda apple touch kuvia sekä html määritteen',
    'shareTheLove'                  => 'helppoa, eikö niin? jaa tämä mahtava sivusto kavereillesi',
    'instructionsTitle'             => 'faviconit-ohjeet',
    'instructionThanks'             => 'kiitos kun käynti faviconit työkalua!',
    'instructions'                  => "kopio tiedostoja sinun nettisivustosi ja lisää tämä koodi HTML <HEAD> tagien sisään:",
    'instructionsBlock'             => 'faviconit.com kuvat',
    'helpUsTranslate'               => 'Auta kääntämään :)',
    'pageNotFound'                  => 'hups.. sivua jota etsit ei voida näyttää',
    'uploadError'                   => 'virhe kuvaa tallentaessa. Yritä uudelleen.',
    'metaDescription'               => 'lataa kuvasi ja tee siitä favicon & apple touch iconien joka toimii kaikilla laiteilla ja selaimille.',
    'metaDownload'                  => 'lataa faviconit sekä html tunnisteen sisältävä zip tiedosto',
    'metaFileNotFound'              => 'tiedosto ei ole saatavilla, lataa tiedosto jotta voit luoda apple touch kuvat sekä html tunnisteen',
    'ogTitle'                       => 'ilmainen favicon & apple touch kuvan luoja',
    'ogDescription'                 => 'lataa kuvasi ja tee siitä favicon & apple touch iconien joka toimii kaikilla laiteilla ja selaimille.',
    'socialImage'                   => "ilmainen favicon & apple touch ikonien\nluoja kaille laitteille ja selaimille",
);
