<?php

/*
|--------------------------------------------------------------------------
| User Details Filter
|--------------------------------------------------------------------------
|
| Log the user details
|
*/
Route::filter('logUserDetails', function () {
    UserHelper::logUserDetails();

});

/*
|--------------------------------------------------------------------------
| User Event Filter
|--------------------------------------------------------------------------
|
| Log the user actions
|
*/
//Route::filter('logUserEvent', function () {
//    $route = Route::currentRouteName();
//    LogHelper::logUserEvent($route);
//});

/*
|--------------------------------------------------------------------------
| CSRF Protection Filter
|--------------------------------------------------------------------------
|
| The CSRF filter is responsible for protecting your application against
| cross-site request forgery attacks. If this special token in a user
| session does not match the one given in this request, we'll bail.
|
*/

Route::filter('csrf', function () {
    if (Session::token() != Input::get('_token')) {
        throw new Illuminate\Session\TokenMismatchException;
    }
});