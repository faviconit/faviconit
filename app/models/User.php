<?php

class User extends Eloquent
{
    public function favicons()
    {
        return $this->hasMany('Favicon');
    }
}