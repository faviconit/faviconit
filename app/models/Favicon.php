<?php

class Favicon extends Eloquent
{
    public function user()
    {
        return $this->belongsTo('User');
    }
}