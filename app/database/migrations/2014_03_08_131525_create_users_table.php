<?php

use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
        Schema::create('users', function($table)
        {
            $table->increments('id');
            $table->string('hash', 64);
            $table->string('ip', 15);
            $table->text('user_agent');
            $table->text('referer');
            $table->string('languages', 256);
            $table->string('advertising', 256);

            $table->timestamps();

            $table->unique('hash');
            $table->index('hash');
        });
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
        Schema::dropIfExists('users');
	}

}