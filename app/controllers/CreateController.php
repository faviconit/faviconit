<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 01/10/13
 * Time: 7:41PM
 * To change this template use File | Settings | File Templates.
 */

class CreateController extends BaseController
{
    //TODO Ajax in case of error
    /**
     * Receives the favicon creation POST
     *
     * @return \Illuminate\Http\RedirectResponse Reload page in case of error or redirect to the download page
     */
    public function postIndex()
    {
        $iconName       = Input::get('iconName');
        $iconFile       = Input::get('iconImage');
        $aviaryIconFile = Input::get('aviaryIconImage');
        $iconVersion    = Input::get('iconVersion');
        $iconLocation   = Input::get('iconLocation');

        $iconFile = FileHelper::moveAviaryImage($aviaryIconFile, $iconFile);

        $data = array(
            'iconName'     => $iconName,
            'iconVersion'  => $iconVersion,
            'iconImage'    => $iconFile,
            'iconLocation' => $iconLocation
        );

        $rules = array(
            'iconName'     => 'max:64|regex:/^[a-zA-Z0-9\.\-\_]*$/',
            'iconVersion'  => 'max:6|regex:/^[a-zA-Z0-9\.\-\_]*$/',
            'iconLocation' => 'regex:/^[a-zA-Z0-9\/\:\.\-\_\?\+]*$/',
            'iconImage'    => 'required|image|squaredImage|minimumSizeImage'
        );

        $validator = Validator::make($data, $rules);

        if ($validator->fails()) {
            $errors = $validator->messages();

            return Redirect::to(LaravelLocalization::getCurrentLocale())->withErrors($errors)->withInput(Input::except('iconImage'));

        } else {
            $iconName = $iconName != '' ? $iconName : 'favicon';

            $relativeWorkFolder = ZipHelper::createZip($iconName, $iconVersion, $iconFile, $iconLocation);
            $zipPath            = $relativeWorkFolder . '/faviconit.zip';
            $responseData       = array('downloadUrl'    => $zipPath,
                                        'socialImageUrl' => $relativeWorkFolder . '/socialShare.png');
            $url                = FileHelper::getWorkFolderName($zipPath);

            return Redirect::to(LaravelLocalization::getCurrentLocale() . '/download/' . $url)->with($responseData);
        }
    }
}