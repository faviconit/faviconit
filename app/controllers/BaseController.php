<?php

/**
 * Class BaseController
 */
class BaseController extends Controller
{

    /**
     * Log user events to all controllers
     */
    public function __construct()
    {
//        $this->beforeFilter('logUserEvent');
    }

    /**
     * Setup the layout used by the controller.
     *
     * @return void
     */
    protected function setupLayout()
    {
        if (!is_null($this->layout)) {
            $this->layout = View::make($this->layout);
        }
    }

}