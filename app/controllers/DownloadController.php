<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 05/12/13
 * Time: 5:39AM
 * To change this template use File | Settings | File Templates.
 */

class DownloadController extends BaseController
{

    public function getIndex($folder = '')
    {
        if ($folder != '' && file_exists('./uploads/' . $folder . '/faviconit.zip')) {
            $data = array(
                'downloadUrl'       => URL::to('uploads/' . $folder . '/faviconit.zip'),
                'socialImageUrl'    => URL::to('uploads/' . $folder . '/socialShare.png'),
                'metaDescription'   => trans('faviconit.metaDownload'),
                'contentGroup1Name' => 'Download Page',
            );
        } else {
            $data = array(
                'metaDescription'   => trans('faviconit.metaFileNotFound'),
                'contentGroup1Name' => 'Empty Download Page',
            );
        }

        return View::make('download', $data);
    }
}