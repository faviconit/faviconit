<?php

class HomeController extends BaseController
{
    /**
     * Log the user details if it's a new user (user not set in the session) or log the event (user already set in the
     * session)
     */
    public function __construct()
    {
//        if (!Session::has('user')) {
        $this->beforeFilter('logUserDetails');
//        } else {
//            $this->beforeFilter('logUserEvent');
//        }
    }

    public function getIndex()
    {
        $data = array(
            'metaDescription'   => trans('faviconit.metaDescription'),
            'contentGroup1Name' => 'Home Page',
        );

        return View::make('home', $data);
    }

}