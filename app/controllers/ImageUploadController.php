<?php
/**
 * Class ImageUploadController
 * Receive the Ajax Uploader File
 */
class ImageUploadController extends BaseController
{
    /**
     * Receives the Ajax POST
     *
     * @return \Illuminate\Http\JsonResponse Ajax message to the view with the result
     */
    public function postIndex()
    {
        $iconFile  = Input::file('iconFile');
        $data      = array('iconFile' => $iconFile);
        $rules     = array('iconFile' => 'required|image|minimumSizeImage');
        $validator = Validator::make($data, $rules);

        //Check the validations (required, image and minimumSizeImage)
        if ($validator->fails()) {
            $response = array('message' => 'error', 'error' => json_decode($validator->messages()));

        } else {
            // Verify if image has been moved with success and then, if image is squared and need aviary
            $fullPath = FileHelper::createUniqueFolderAndMoveFile($iconFile);

            if ($fullPath === false) {
                Log::error('error when trying to save the image. please, try again.');
                $error    = array("iconFile" => array(trans('faviconit.uploadError')));
                $response = array('message' => 'error', 'error' => $error);

            } elseif (ImageHelper::imageIsSquared($fullPath)) {
                $response = array('message' => 'squared', 'path' => $fullPath);


            } else {
                $response = array('message' => 'notSquared', 'path' => $fullPath);

            }
        }
//        DEBUG
//        sleep(1000);
        return Response::json(json_encode($response), 200);
    }
}