<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 01/12/13
 * Time: 8:13AM
 * To change this template use File | Settings | File Templates.
 */

class FileHelper
{
    /**
     * Create a unique folder using DateTime and the file SHA1
     *
     * @param $file File The path of the uploaded file in the temporary folder
     * @return bool|string The moved file to the final folder or false in case of error
     */
    public static function createUniqueFolderAndMoveFile($file)
    {
        if (isset($file)) {
            $date            = new DateTime();
            $datetime        = $date->getTimestamp();
            $userId          = Session::get('user')->hash;
            $destinationPath = 'uploads/' . $userId . $datetime;
            $newName         = 'favicon.' . $file->guessExtension();
            $uploadSuccess   = $file->move($destinationPath, $newName);

            $fullPath = $destinationPath . '/' . $newName;

            if ($uploadSuccess) {
                return $fullPath;
            }
        }

        return false;
    }

    /**
     * Move the Aviary generated image to the local public path
     *
     * @param $aviaryIconFile string The Aviary file URL
     * @param $iconFile       string The current uploaded image URL
     * @return null|string The new file path or null if the iconFile was not setted
     */
    public static function moveAviaryImage($aviaryIconFile, $iconFile)
    {
        if ($iconFile != '') {
            $iconFileLocalPath = public_path() . str_replace(URL::to('/'), '', $iconFile);

            // Copy Aviary file if it was used
            if ($aviaryIconFile != '') {
                static::saveExternalImage($aviaryIconFile, $iconFileLocalPath);
            }

            return $iconFileLocalPath;
        }

        return null;
    }

    /**
     * Download an external image and saves it to a local folder
     *
     * @param $imageUrl string The URL of the external image
     * @param $path     string The path where the image will be saved
     */
    private static function saveExternalImage($imageUrl, $path)
    {
        $ch = curl_init($imageUrl);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER, 1);

        $rawData = curl_exec($ch);
        curl_close($ch);

        if (file_exists($path)) {
            unlink($path);
        }

        $fp = fopen($path, 'x');
        fwrite($fp, $rawData);
        fclose($fp);
    }

    /**
     * Get the relative path to the image path folder (work folder)
     *
     * @param $filePath string The full path of the image
     * @return string The relative path where the image is stored
     */
    public static function getRelativeWorkFolder($filePath)
    {
        $dirArray   = explode('/', $filePath);
        $workFolder = 'uploads/' . $dirArray[sizeof($dirArray) - 2];

        return $workFolder;
    }

    /**
     * Get the absolute path to the image path folder (work folder)
     *
     * @param $filePath string The full path of the image
     * @return string The absolute path where the image is stored
     */
    public static function getAbsoluteWorkFolder($filePath)
    {
        $dirArray   = explode('/', $filePath);
        $workFolder = '';
        for ($i = 0; $i < sizeof($dirArray) - 1; $i++) {
            $workFolder .= $dirArray[$i] . '/';
        }

        return $workFolder;
    }

    /**
     * Get the name of the work folder
     *
     * @param $filePath string The full path of the image
     * @return string The folder name where the image is stored
     */
    public static function getWorkFolderName($filePath)
    {
        $dirArray   = explode('/', $filePath);
        $workFolder = $dirArray[sizeof($dirArray) - 2];

        return $workFolder;
    }

    /**
     * Delete the icon files from an work folder
     *
     * @param array  $sizes             The list of sizes
     * @param string $workFolder        The folder where the images were converted
     * @param string $imgName           The name of the image
     * @param string $originalImagePath The path to the original image
     */
    public static function deleteIconFiles(array $sizes, $workFolder, $imgName, $originalImagePath = '')
    {
        foreach ($sizes as $size) {
            unlink("$workFolder/$imgName-$size.png");
        }
        unlink("$workFolder/$imgName.ico");
        unlink($originalImagePath);
    }
}