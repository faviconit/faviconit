<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 01/12/13
 * Time: 8:13AM
 * To change this template use File | Settings | File Templates.
 */

class IpHelper
{
    /**
     * Get user IP
     *
     * @return string IP
     */
    public static function getIp()
    {
        // The last 'else' is used if all local tests for an external IP fails
        if (isset($_SERVER["HTTP_X_FORWARDED_FOR"])) {
            Log::debug("HTTP_X_FORWARDED_FOR");
            $ip = $_SERVER["HTTP_X_FORWARDED_FOR"];

        } elseif (isset($_SERVER["HTTP_CLIENT_IP"])) {
            Log::debug("HTTP_CLIENT_IP");
            $ip = $_SERVER["HTTP_CLIENT_IP"];

        } else {
            Log::debug("REMOTE_ADDR");
            $ip = $_SERVER["REMOTE_ADDR"];
        }

        return strval($ip);
    }
}