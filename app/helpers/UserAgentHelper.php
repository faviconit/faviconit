<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 01/12/13
 * Time: 8:13AM
 * To change this template use File | Settings | File Templates.
 */

class UserAgentHelper
{
    /**
     * Get user agent
     *
     * @return string user agent
     */
    public static function getUserAgent()
    {
        if (isset($_SERVER['HTTP_USER_AGENT'])) {
            $userAgent = $_SERVER['HTTP_USER_AGENT'];
        } else {
            $userAgent = 'not set';
        }

        return $userAgent;
    }
}