<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 04/12/13
 * Time: 6:02AM
 * To change this template use File | Settings | File Templates.
 */

class InstructionsHelper
{

    /**
     * Create and return the instructions to be included in the zip file
     *
     * @param string $imgName The name of the images
     * @param string $version The version
     * @return string The instructions
     */
    public static function getInstructions($imgName, $version, $iconLocation)
    {
        $thanks         = trans('faviconit.instructionThanks');
        $instruction    = trans('faviconit.instructions');
        $faviconitTitle = trans('faviconit.instructionsBlock');

        $iconLocation = rtrim($iconLocation, '/');
        $instructions = <<<HTML
$thanks
$instruction

	<!-- ****** $faviconitTitle ****** -->
	<link rel="shortcut icon" href="$iconLocation/$imgName.ico$version">
	<link rel="icon" sizes="16x16 32x32 64x64" href="$iconLocation/$imgName.ico$version">
	<link rel="icon" type="image/png" sizes="196x196" href="$iconLocation/$imgName-192.png$version">
	<link rel="icon" type="image/png" sizes="160x160" href="$iconLocation/$imgName-160.png$version">
	<link rel="icon" type="image/png" sizes="96x96" href="$iconLocation/$imgName-96.png$version">
	<link rel="icon" type="image/png" sizes="64x64" href="$iconLocation/$imgName-64.png$version">
	<link rel="icon" type="image/png" sizes="32x32" href="$iconLocation/$imgName-32.png$version">
	<link rel="icon" type="image/png" sizes="16x16" href="$iconLocation/$imgName-16.png$version">
	<link rel="apple-touch-icon" href="$iconLocation/$imgName-57.png$version">
	<link rel="apple-touch-icon" sizes="114x114" href="$iconLocation/$imgName-114.png$version">
	<link rel="apple-touch-icon" sizes="72x72" href="$iconLocation/$imgName-72.png$version">
	<link rel="apple-touch-icon" sizes="144x144" href="$iconLocation/$imgName-144.png$version">
	<link rel="apple-touch-icon" sizes="60x60" href="$iconLocation/$imgName-60.png$version">
	<link rel="apple-touch-icon" sizes="120x120" href="$iconLocation/$imgName-120.png$version">
	<link rel="apple-touch-icon" sizes="76x76" href="$iconLocation/$imgName-76.png$version">
	<link rel="apple-touch-icon" sizes="152x152" href="$iconLocation/$imgName-152.png$version">
	<link rel="apple-touch-icon" sizes="180x180" href="$iconLocation/$imgName-180.png$version">
	<meta name="msapplication-TileColor" content="#FFFFFF">
	<meta name="msapplication-TileImage" content="$iconLocation/$imgName-144.png$version">
	<meta name="msapplication-config" content="$iconLocation/browserconfig.xml">
	<!-- ****** $faviconitTitle ****** -->
HTML;

        return $instructions;
    }
}
