<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 01/12/13
 * Time: 8:13AM
 * To change this template use File | Settings | File Templates.
 */

class RefererHelper
{
    /**
     * Get referer
     *
     * @return string referer
     */
    public static function getReferer()
    {
        if (isset($_SERVER['HTTP_REFERER'])) {
            $referer = strval($_SERVER['HTTP_REFERER']);
        } else {
            $referer = 'no referer set';
        }

        return $referer;
    }
}