<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 03/12/13
 * Time: 7:31AM
 * To change this template use File | Settings | File Templates.
 */

class UserHelper
{
    public static function logUserDetails()
    {
        if (Session::has('user')) { //user set on Session
            $user = Session::get('user');

        } else { //user not set on Session
            $ip        = IpHelper::getIp();
            $userAgent = UserAgentHelper::getUserAgent();
            $hash      = hash('sha256', $ip . $userAgent);


            //try to fech user from DB
            $user = User::where('hash', '=', $hash)->get()->first();

            //if user not set on DB, create user
            if (null == $user) {
                $user             = new User();
                $user->hash       = $hash;
                $user->ip         = $ip;
                $user->user_agent = $userAgent;
                $user->referer    = RefererHelper::getReferer();
                $languages        = '';
                foreach (Request::getLanguages() as $lang) {
                    $languages .= $lang . ', ';
                }

                $user->languages   = $languages;
                $user->advertising = 'top';
                $user->save();
            }
        }
        //add or update user to Session
        Session::set('user', $user);
    }

//    public static function logUserEvent()
//    {
//        $id    = USER_ID;
//        $route = URL::current();
//        $log   = <<<LOG
//
//user '$id'
//    URL => $route
//LOG;
//
//        Log::info($log);
//    }

    public static function logNewFavicon($filePath)
    {
        $user    = Session::get('user');
        $favicon = new Favicon();

        $dirArray            = explode('/', $filePath);
        $filePath            = $dirArray[sizeof($dirArray) - 2];
        $favicon->image_path = $filePath;

        $user->favicons()->save($favicon);
    }
}