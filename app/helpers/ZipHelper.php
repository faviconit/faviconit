<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 03/12/13
 * Time: 6:56AM
 * To change this template use File | Settings | File Templates.
 */

class ZipHelper
{
    /**
     * Creates a Zip file and returns it's path
     *
     * @param $imgName string The favicon name
     * @param $version string The favicon version
     * @param $imgPath string The favicon image path
     * @param $iconLocation string The favicon location
     * @return string The relative work folder
     */
    public static function createZip($imgName, $version, $imgPath, $iconLocation)
    {
        $workFolder = FileHelper::getAbsoluteWorkFolder($imgPath);

        Artisan::call('createAndZip', array(
                'imgName'      => $imgName,
                'imgPath'      => $imgPath,
                'workFolder'   => $workFolder,
                'version'      => $version,
                'iconLocation' => $iconLocation,
            )
        );

        $relativeWorkFolder = FileHelper::getRelativeWorkFolder($imgPath);

        UserHelper::logNewFavicon($imgPath);

        return $relativeWorkFolder;
    }
}