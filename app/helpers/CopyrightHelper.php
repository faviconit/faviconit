<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 01/12/13
 * Time: 8:13AM
 * To change this template use File | Settings | File Templates.
 */

class CopyrightHelper
{

    /**
     * Return the correct copyright year string
     *
     * @return string Copyright year string
     */
    public static function getYearString()
    {
        $currentYear = date('Y');
        $launchYear  = 2013;

        if ($currentYear == $launchYear) {
            $copyrightYear = (string)$launchYear;
        } else {
            $copyrightYear = $launchYear . '-' . $currentYear;
        }

        return $copyrightYear;

    }
}