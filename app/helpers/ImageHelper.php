<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 01/12/13
 * Time: 8:46AM
 * To change this template use File | Settings | File Templates.
 */

class ImageHelper
{
    /**
     * Check if an image height and width are equal
     *
     * @param $imagePath string The path to the image
     * @return bool True height and width are equal
     */
    public static function imageIsSquared($imagePath)
    {
        Log::debug($imagePath);
        if (function_exists('getimagesize') && $imagePath != '') {
            $imageInfo   = getimagesize($imagePath);
            $imageWidth  = $imageInfo[0];
            $imageHeight = $imageInfo[1];
            if ($imageWidth != $imageHeight) {
                return false;
            } else {
                return true;
            }
        }
        Log::error('getimagesize do not exist');

        return false;
    }

    /**
     * Check if an image is bigger than the minimum defined size (310px in each dimension)
     *
     * @param $imagePath string The path to the image
     * @return bool True if image size is equal or bigger than the minimum defined size
     */
    public static function imageHasTheNeededSize($imagePath)
    {
        $minimumSize = 310;
        Log::debug($imagePath);
        if (function_exists('getimagesize')) {
            $imageInfo   = getimagesize($imagePath);
            $imageWidth  = $imageInfo[0];
            $imageHeight = $imageInfo[1];
            if ($imageWidth < $minimumSize || $imageHeight < $minimumSize) {
                return false;
            } else {
                return true;
            }
        }
        Log::error('getimagesize do not exist');

        return false;
    }
}