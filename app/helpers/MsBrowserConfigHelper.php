<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 04/12/13
 * Time: 6:02AM
 * To change this template use File | Settings | File Templates.
 */

class MsBrowserConfigHelper
{

    /**
     * Create and return the Microsoft Windows 8 Browser Config
     *
     * @param string $imgName The name of the images
     * @return string browserconfig.xml text
     */
    public static function getMsBrowserConfig($imgName)
    {
        $instructions = <<<XML
<?xml version="1.0" encoding="utf-8"?>
<browserconfig>
  <msapplication>
    <tile>
      <square70x70logo src="/$imgName-70.png"/>
      <square150x150logo src="/$imgName-150.png"/>
      <square310x310logo src="/$imgName-310.png"/>
      <TileColor>#FFFFFF</TileColor>
    </tile>
  </msapplication>
</browserconfig>
XML;

        return $instructions;
    }
}