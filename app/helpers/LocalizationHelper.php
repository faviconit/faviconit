<?php
/**
 * Created by JetBrains PhpStorm.
 * User: edu
 * Date: 01/12/13
 * Time: 8:13AM
 * To change this template use File | Settings | File Templates.
 */

class LocalizationHelper
{
    /**
     * To use localization in an easier way, it's set as 'pt' instead of 'pt_BR'. To get the correct Brazilian
     * localization in aviary, this function return 'pt_BR' when locale is set to 'pt'
     *
     * It also set Aviary to English if the locale is note supported (Romanian)
     *
     * @return string The correct Aviary locale string
     */
    public static function getAviaryLocale()
    {
        $appLocale = Lang::getLocale();
        $locale = $appLocale;

        if ($appLocale == 'pt')
            $locale = 'pt_BR';
        elseif ($appLocale == 'ro')
            $locale = 'en';

        return $locale;
    }

    public static function getSocialLocale()
    {
        $socialLocale = Config::get('app.languages');

        return $socialLocale[Lang::getLocale()];
    }

    public static function getUserLanguageFromSupportedLanguages()
    {
        $userLanguages      = Request::getLanguages();
        $supportedLanguages = Config::get('app.languages');
        $userLanguage       = Config::get('app.locale');
        foreach ($userLanguages as $language) {
            $language = substr($language, 0, 2);

            if (false !== array_search($language, $supportedLanguages, true)) {
                $userLanguage = $language;
                break;
            }
        }

        return $userLanguage;
    }

    /**
     * Returns html with language selector. This code removes languages without URL.
     * @return \Illuminate\View\View
     */
    public static function getLanguageSelector()
    {
        //START - Delete in v1.0
        $languages = LaravelLocalization::getSupportedLocales();
        $active    = LaravelLocalization::getCurrentLocale();

        foreach ($languages as $localeCode => $language) {
            $langUrl = LaravelLocalization::getLocalizedURL($localeCode);

            // check if the url is set for the language
            if ($langUrl) {
                $language['url'] = $langUrl;
            } else {
                // the url is not set for the language (check lang/$lang/routes.php)
                unset($languages[$localeCode]);
            }
            // fill the active language with it's data
            if ($active == $localeCode)
                $native = $language['native'];
        }

        return View::make('templates.languagebar', compact('languages', 'active', 'native'));
    }
}