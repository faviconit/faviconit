<?php

class ImageValidator extends \Illuminate\Validation\Validator
{
    public function validateSquaredImage($attribute, $value, $parameters)
    {
        return ImageHelper::imageIsSquared($value);
        Log::error('$value or attribute not set');

        return false;

    }

    public function validateMinimumSizeImage($attribute, $value, $parameters)
    {
        return ImageHelper::imageHasTheNeededSize($value);
        Log::error('$value or attribute not set');

        return false;

    }
}