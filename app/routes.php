<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the Closure to execute when that URI is requested.
|
*/

Route::group(
    array('prefix' => LaravelLocalization::setLocale(),
          'before' => 'LaravelLocalizationRedirectFilter',
    ),
    function () {
        Route::controller('upload', 'ImageUploadController');
        Route::controller('faviconit', 'CreateController');
        Route::controller('download/{folder?}', 'DownloadController');
        Route::controller('/', 'HomeController');
    }
);
