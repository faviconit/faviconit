<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputOption;

class CreateAndZip extends Command
{

    /**
     * The console command name.
     *
     * @var string
     */
    protected $name = 'createAndZip';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create the icons and Zip it.';

    /**
     * Create a new command instance.
     *
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return void
     */
    public function fire()
    {
        $imgName      = $this->argument('imgName');
        $imgPath      = $this->argument('imgPath');
        $workFolder   = $this->argument('workFolder');
        $version      = $this->argument('version') != '' ? '?v=' . $this->argument('version') : '';
        $iconLocation = $this->argument('iconLocation');

        //
        $sizes = array(310, 192, 180, 160, 152, 150, 144, 120, 114, 96, 76, 72, 70, 64, 60, 57, 32, 16);

        $zip     = new ZipArchive();
        $zipName = "$workFolder/faviconit.zip";


        if ($zip->open($zipName, ZipArchive::CREATE) !== true) {
            dd("ERROR");
        }

        if (App::environment() == 'local') {
//            $convert = '/usr/local/bin/convert';
            $convert     = '/opt/imagemagick-6.8/bin/convert';
            $bgImagePath = '/vagrant/www/app/commands/bg.png';
            $sudo        = '';

        } elseif (App::environment() == 'homolog') {
            // $convert = '/opt/imagemagick-6.8/bin/convert';
            $convert     = '/usr/bin/convert';
            $bgImagePath = '/var/www/homolog/app/commands/bg.png';
            $sudo        = '';

        } else {
//            $convert = '/opt/imagemagick-6.8/bin/convert';
            $convert     = '/usr/bin/convert';
            $bgImagePath = '/var/www/homolog/app/commands/bg.png';
            $sudo        = 'sudo ';
        }

        exec($sudo . 'chmod -R 777 ' . $workFolder);

        $alpha         = '-alpha On';
        $setColorspace = '-set colorspace sRGB';
        $colorspace    = '-colorspace sRGB';
        $type          = '-type TrueColorMatte';

        foreach ($sizes as $size) {
            $resize         = '-resize ' . $size . 'x' . $size;
            $dinamicUnsharp = '-unsharp ' . '0x' . (1 / $size * 8);
            $favicon        = "$imgName-$size.png";
            $faviconPath    = "$workFolder/$favicon";
            $convertCommand = "$convert $imgPath $setColorspace $resize $alpha $dinamicUnsharp $setColorspace $type $faviconPath";

            exec($convertCommand);
            $zip->addFile($faviconPath, $favicon);
        }

        $icoCommand = "$convert $workFolder/$imgName-64.png $workFolder/$imgName-32.png " .
            "$workFolder/$imgName-16.png $workFolder/$imgName.ico";

        exec($icoCommand);
        $zip->addFile("$workFolder/$imgName.ico", "$imgName.ico");

        $instructions = InstructionsHelper::getInstructions($imgName, $version, $iconLocation);
        $zip->addFromString(trans('faviconit.instructionsTitle') . '.txt', $instructions);
        $msBrowserConfig = MsBrowserConfigHelper::getMsBrowserConfig($imgName);
        $zip->addFromString('browserconfig.xml', $msBrowserConfig);

        $zip->close();

        // Create the social image
        $text                    = trans('faviconit.socialImage');
        $text                    = "";
        $lang                    = LaravelLocalization::getCurrentLocale();
        $font                    = "OpenSans";
        if ($lang === 'ar') {
            $font = "Lateef-Regular";
        } elseif ($lang == 'zh' || $lang === 'el') {
            $text = "";
        }

        $transparentImageCommand = <<<SIC
$convert -size 560x292 xc:none \
$workFolder/$imgName-160.png -geometry +15+58 \
-composite $workFolder/$imgName-120.png -geometry +200+78 \
-composite $workFolder/$imgName-76.png -geometry +340+100 \
-composite $workFolder/$imgName-57.png -geometry +431+110 \
-composite $workFolder/$imgName-32.png -geometry +498+122 \
-composite $workFolder/$imgName-16.png -geometry +535+130 \
-composite -font $font -kerning -1 -pointsize 18 -fill White -annotate +15+260 "$text" \
$workFolder/transp.png
SIC;
        $dropShadowCommand       = <<<DSC
$convert $workFolder/transp.png \( +clone -background black -shadow 70x2+3+3 \) \+swap -background none \
-layers merge +repage $workFolder/comp.png
DSC;
        $finalImageCommand       = <<<BGC
$convert -size 560x292 -composite $bgImagePath $workFolder/comp.png $workFolder/socialShare.png
BGC;
        exec($transparentImageCommand);
        exec($dropShadowCommand);
        unlink("$workFolder/transp.png");
        exec($finalImageCommand);
        unlink("$workFolder/comp.png");

        FileHelper::deleteIconFiles($sizes, $workFolder, $imgName, $imgPath);

        return $zipName;
    }

    /**
     * Get the console command arguments.
     *
     * @return array
     */
    protected function getArguments()
    {
        return array(
            array('imgName', InputArgument::REQUIRED, 'Name used to convert and create the Zip'),
            array('imgPath', InputArgument::REQUIRED, 'Path to the original image'),
            array('workFolder', InputArgument::REQUIRED, 'Folder used to create the files'),
            array('version', InputArgument::OPTIONAL, 'Cache avoidance version'),
            array('iconLocation', InputArgument::OPTIONAL, 'Favicon final location'),
        );
    }

    /**
     * Get the console command options.
     *
     * @return array
     */
    protected function getOptions()
    {
        return array(
            array('nope', null, InputOption::VALUE_OPTIONAL, 'This command has no options :)', null),
        );
    }

}
