<?php

use Illuminate\Console\Command;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Input\InputArgument;

class DeleteOldFiles extends Command {

	/**
	 * The console command name.
	 *
	 * @var string
	 */
	protected $name = 'deleteOldFiles';

	/**
	 * The console command description.
	 *
	 * @var string
	 */
	protected $description = 'Delete files older than 1 hour';

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		parent::__construct();
	}

	/**
	 * Execute the console command.
	 *
	 * @return void
	 */
	public function fire()
	{
		$now = date(DATE_W3C);
		$folders = File::directories('./public/uploads');

		foreach($folders as $folder)
		{
			$file = File::files($folder);
			$file = $file[0];

			$modified = date(DATE_W3C, File::lastModified($file));

			$delta = (strtotime($now) - strtotime($modified))/3600;

			if($delta >= 1)
			{
				File::deleteDirectory($folder);
			}
		}
	}

	/**
	 * Get the console command arguments.
	 *
	 * @return array
	 */
	protected function getArguments()
	{
		return array();
	}

	/**
	 * Get the console command options.
	 *
	 * @return array
	 */
	protected function getOptions()
	{
		return array();
	}

}