# Translation

Edit the following files:

- ```app\config\app.php```
- ```app\config\packages\mcamara\laravel-localization\config.php```

Copy the language from the folder ```app\lang\Laravel4-lang-master```

You will use only the ```validation.php``` file.

Use the translation to create the ```faviconit.php``` (copy from the English one)

REMEMBER

Check if the flag exists in ```public\assets\css\faviconit.css``` and change the assetsVersion in ```app\config\app.php```


OTHER

Sometimes it's necessary to edit ```app\helpers\LocalizationHelper.php``` if Aviary doesn't support the language
https://rtlcss.com/ for RTL languages
